const mDeviceIdlers = require('./dbApiTests/mDeviceIdlers')
const mIdlerTimes = require('./dbApiTests/mIdlerTimes')
const mInstanceItem = require('./dbApiTests/mInstanceItem')
const xIdleSession = require('./dbApiTests/xIdleSession')
const { createWatchTimesForMissingSteamid64s } = require('./droplogger')
const { getDropWeek } = require('./helpers')
const { asyncCursor, saveBulk } = require('./mongo')
const events = new (require('events')).EventEmitter()

const log = require('pino')({ prettyPrint: { colorize: true, translateTime: true } }).child({ name: 'migrations' })

const dates = {
  LEADERBOARDS_TOTAL_WORTH: 1604320906000,
  DEVICEDROPS_TOTAL_WORTH: 1604320906000,
  LEADERBOARD_HISTORICAL: 1604323309288
}

module.exports.events = events

module.exports.doMigrations = async function doMigrations() {
  const start = Date.now()
  log.info('Starting migrations')
  // await generateIdledMinutesFromXIdleSessions()
  // await removeNonStandardInstanceItems()
  // await createMissingWatchTimesForIdlers()
  log.info(`migrations completed in ${Date.now() - start}ms`)
  events.emit('complete')
}

async function generateIdledMinutesFromXIdleSessions() {
  const idlerTimesDocsByWeek = {}
  const count = await xIdleSession.countDocuments().exec()
  let progress = 0
  const interval = setInterval(() => {
    log.info(`generateIdledMinutesFromXIdleSessions || processed ${progress} out of ${count} xIdleSessions`)
  }, 10000)
  await asyncCursor('', xIdleSession.find({}, { week: 1, idlers: 1, duration: 1, launch: 1 }).lean(), async doc => {
    progress++
    for (let i = 0; i < doc.idlers.length; i++) {
      const steamid64 = doc.idlers[i]
      let byWeek = idlerTimesDocsByWeek[doc.week]
      if (!byWeek) {
        byWeek = []
        idlerTimesDocsByWeek[doc.week] = byWeek
      }

      if (!byWeek[steamid64]) {
        byWeek[steamid64] = 0
      }
      byWeek[steamid64] += doc.duration - (doc.launch || 0)
    }
  }, { parallel: 10 })
  clearInterval(interval)
  const arr = Object.keys(idlerTimesDocsByWeek)
  for (let i = 0; i < arr.length; i++) {
    const weekNumber = arr[i]
    const idlerTimes = idlerTimesDocsByWeek[weekNumber]
    await saveBulk(Object.keys(idlerTimes).map(steamid64 => { return { week: Number(weekNumber), steamid64, minutes: idlerTimes[steamid64] } }), ['week', 'steamid64'], mIdlerTimes)
    log.info(`saved idler times for week ${weekNumber}`)
  }
}

async function removeNonStandardInstanceItems() {
  await mInstanceItem.deleteMany({ defindex: { $exists: true } }).exec()
}

async function createMissingWatchTimesForIdlers() {
  const devices = await mDeviceIdlers.find({}).lean().exec()
  const steamid64s = {}
  for (let i = 0; i < devices.length; i++) {
    const device = devices[i];
    await createWatchTimesForMissingSteamid64s(device.steamid64s)
  }
}