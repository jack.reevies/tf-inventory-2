const { asyncCursor } = require('./mongo')
const mPrice = require('./dbApiTests/mPrice')
const mSpecificIdItem = require('./dbApiTests/mSpecificIdItem')
const MISSING_IMAGE_URL = 'https://steamcommunity-a.akamaihd.net/economy/image/-9a81dlWLwJ2UUGcVs_nsVtzdOEdtWwKGZZLQHTxH5rd9eDAjcFyv45SRYAFMIcKL_PArgVSL403ulRUWEndVKv5h5-DHA8hdAACte3zegU4h6CdcG4b6Nqyl9HalqOjN72GkD0A7Z1wj7-Q89y7mFqzq0hk6Tg'
const { SPECIFIC_IDS_METALS } = require('./helpers')
const ItemSchema = require('@spacepumpkin/tf-itemschema')

/**
 * Takes an array of item objects (with property 'specfici_id'). Attaches prices to them by setting obj.price
 * @param {{specific_id}[]} items
 * @returns {totalWorth: Number, metalWorth: Number} An object containing totalWorth and metalWorth keys representing the metal worth of items pased in
 */
async function attachPricesToItems (items) {
  if (items.length === 0) return { totalWorth: 0, metalWorth: 0, f2pWorth: 0, totalItems: 0, metalItems: 0, f2pItems: 0 }
  let totalWorth = 0
  let metalWorth = 0
  let f2pWorth = 0
  let totalItems = 0
  let metalItems = 0
  let f2pItems = 0
  const prices = await getPricesForSpecificIds(items.map(o => o.specific_id))
  for (let i = 0; i < items.length; i++) {
    const item = items[i]
    const count = item.instances ? Array.isArray(item.instances) ? item.instances.length : Object.keys(item.instances).length : 1
    totalItems += count
    item.price = prices[item.specific_id] || 0
    const addWorth = item.price * count
    if (SPECIFIC_IDS_METALS.indexOf(item.specific_id) > -1) {
      metalWorth += addWorth
      metalItems += count
    }
    if (item.specific_id.indexOf(';untradable') > -1) {
      item.f2pPrice = prices[item.specific_id.replace(';untradable', '')] || 0
      f2pWorth += (item.f2pPrice * count) || 0
      f2pItems += count
    }
    totalWorth += addWorth
  }
  return { totalWorth, metalWorth, f2pWorth, totalItems, metalItems, f2pItems }
}

async function getPricesForSpecificIds (specificIds) {
  const query = { $or: [] }
  const specificIdCache = {}
  for (let i = 0; i < specificIds.length; i++) {
    const specificid = specificIds[i].replace(';untradable', '')
    if (!specificIdCache[specificid]) {
      query.$or.push({ specific_id: specificid })
      specificIdCache[specificid] = 1
    }
  }

  const prices = {}
  await asyncCursor('mPrice', mPrice.find(query), doc => {
    prices[doc._doc.specific_id] = doc._doc.avg_metal
  }, null, true)
  return prices
}

/**
 * Takes an array of item objects (with property 'specfici_id'). Attaches urls to them by setting obj.url
 * @param {{specific_id}[]} items
 */
async function attachUrlsForSpecificIds (items) {
  if (items.length === 0) return
  const urls = await getUrlsForSpecificIds(items.map(o => o.specific_id))
  for (let i = 0; i < items.length; i++) {
    const item = items[i]
    item.url = urls[item.specific_id] || MISSING_IMAGE_URL
  }
}

async function getUrlsForSpecificIds (specificIds) {
  const query = { $or: [] }
  for (let i = 0; i < specificIds.length; i++) {
    const specificid = specificIds[i]
    const existIndex = query.$or.indexOf(specificid)
    if (existIndex === -1) {
      query.$or.push({ specific_id: specificid })
    }
  }
  const urls = {}
  await asyncCursor('mSpecificIdItem', mSpecificIdItem.find(query), doc => {
    urls[doc._doc.specific_id] = doc._doc.url
  }, null, true)
  return urls
}

async function attachSchemaInfoToItems (items) {
  for (let i = 0; i < items.length; i++) {
    const item = items[i]
    const schemaItem = ItemSchema.findSchemaItemFromDefindex(item.defindex)
    if (!schemaItem) continue
    items[i] = { ...schemaItem, ...items[i] }
  }
}

module.exports = {
  attachPricesToItems,
  attachUrlsForSpecificIds,
  attachSchemaInfoToItems
}
