const fetch = require('node-fetch')
const log = require('pino')({ prettyPrint: { colorize: true, translateTime: true } })
const { wait } = require('./helpers')
const TIME_BETWEEN_REQUEST = 1000 * 60 * 5
const RATELIMIT_COOLDOWN = 1000 * 60 * 60 * 12
const ZOMBIE_COOLDOWN = 60000

const stats = {
  debugIntervalsNotMet: [],
  ratelimitTriggers: 0,
  lastSteamRequest: 0,
  successfulRequestsThisMinute: 0,
  requestsInLastMinute: 0,
  requestsInThisMinute: 0,
  requestsPerMinute: {},
  successfulRequestsPerMinute: {},
  lastSuccessfulRequest: 0,
  poolSize: 0,
  excludeIPs: {},  // Record<ip, expiresAt>
  /** @type [{ip, nextRequestAt, lastStatusCode, lastRequestAt, requestsSinceLastRatelimit, lastSuccessfulRequest, successfulRequestsThisMinute, requests, ratelimits, cookieString, lastRateLimitAt, zombie}] */
  knownIPs: [] // {ip, nextRequestAt}
}

const serverStart = Date.now()

function getEpochMinute(time) {
  if (!time) {
    time = Date.now()
  }
  return Math.round((time / 1000) / 60)
}

setImmediateInterval(getCurrentIPPool, 60000)

setInterval(() => {
  const epochMinute = getEpochMinute() - 1
  stats.requestsInLastMinute = stats.requestsInThisMinute
  stats.requestsPerMinute[epochMinute] = stats.requestsInThisMinute
  stats.requestsInThisMinute = 0

  stats.successfulRequestsPerMinute[epochMinute] = stats.successfulRequestsThisMinute
  stats.successfulRequestsThisMinute = 0

  for (const ip of stats.knownIPs) {
    ensureIntervalsAreMet(ip.ip, ip.requests)
  }

}, 60000)

function ensureIntervalsAreMet(ip, requests) {
  for (let i = 1; i < requests.length - 1; i++) {
    const request = requests[i];
    const previousRequest = requests[i - 1]

    if (request - previousRequest < TIME_BETWEEN_REQUEST) {
      stats.debugIntervalsNotMet.push(`${ip} did not wait ${TIME_BETWEEN_REQUEST} between these two requests ${previousRequest} and ${request} (was ${request - previousRequest}ms difference)`)
    }
  }
  requests.splice(0, requests.length - 1)
}

function setImmediateInterval(fn, ms) {
  setInterval(fn, ms)
  fn()
}

async function makeSteamRequest(url, referrer) {
  return { statusCode: 500 }
  let tor = await makeProxyRequest(url, referrer)

  // Disabling this for now because we don't want to ratelimit home IP
  if (tor.rawError?.startsWith("Exhausted")) {
    //tor = await makeNativeRequest(url)
    log.warn(`NO IPS AVAILABLE - REEEEEEEEEE`)
    return { statusCode: 500 }
  }

  return tor
}

function pickBestIP() {
  const availableIPs = stats.knownIPs.filter(o => !o.zombie && o.nextRequestAt < Date.now())
  return availableIPs.sort((a, b) => a.nextRequestAt < b.nextRequestAt ? -1 : 1)[0]
}

async function makeProxyRequest(url, referrer) {
  let targetIP

  while (!targetIP) {
    targetIP = pickBestIP()

    if (targetIP) {
      targetIP.nextRequestAt = Date.now() + TIME_BETWEEN_REQUEST
      targetIP.zombie = false
      break
    }

    await wait(Math.random() * 500 + 100)
  }

  const body = {
    method: 'GET',
    url: url,
    headers: {
      "Accept": "*/*",
      "Accept-Encoding": "gzip, deflate, br",
      "Connection": "keep-alive",
      "Host": "steamcommunity.com",
      "Referrer": referrer || 'https://steamcommunity.com/',
      "Cookie": targetIP.cookieString || "",
      "Sec-Fetch-Dest": "empty",
      "Sec-Fetch-Mode": "cors",
      "Sec-Fetch-Site": "same-origin",
      "Sec-GPC": "1",
      "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36",
      "X-Requested-With": "XMLHttpRequest"
    },
    timeout: 60000,
    maxRedirects: 1,
    successCodes: [200, 429, 500],
    ip: targetIP.ip
  }

  let json

  log.info(`SteamRequest making call to ${url} using ${targetIP.ip} (${getUsableProxies()} usable proxies)`)
  const res = await fetch(`${process.env.FLUX_PROXY_GATEWAY}/request`, {
    method: 'post',
    body: JSON.stringify(body),
    headers: {
      'Content-Type': 'application/json'
    }
  })

  json = await res.json()

  targetIP.requests.push({ time: Date.now(), minute: getEpochMinute(), status: json.statusCode })

  if (!json.statusCode) {
    // Assume IP is dead
    targetIP.zombie = true
    targetIP.nextRequestAt = Date.now() + ZOMBIE_COOLDOWN
    targetIP.lastStatusCode = 0
    return await makeProxyRequest(url, referrer)
  }

  targetIP.nextRequestAt = Date.now() + TIME_BETWEEN_REQUEST
  targetIP.requestsSinceLastRatelimit++

  if (json.statusCode === 200) {
    targetIP.lastSuccessfulRequest = Date.now()
    targetIP.successfulRequestsThisMinute++
    stats.lastSuccessfulRequest = Date.now()
    stats.successfulRequestsThisMinute++
  }

  if (json.statusCode === 429) {
    stats.ratelimitTriggers++
    targetIP.nextRequestAt = Date.now() + RATELIMIT_COOLDOWN
    targetIP.ratelimits.push({ time: Date.now(), minute: getEpochMinute(), length: Date.now() - (targetIP.lastRateLimitAt || serverStart), requests: targetIP.requestsSinceLastRatelimit })
    targetIP.requestsSinceLastRatelimit = 0
    targetIP.lastRateLimitAt = Date.now()
    log.warn(`Got 429 on ${targetIP.ip} ${targetIP.lastStatusCode === 429 ? 'AGAIN' : ''} - BANNING FOR ${RATELIMIT_COOLDOWN / 60000} MINUTES`)
  }

  if (json.statusCode === 500) {
    log.warn(`${JSON.stringify(json.body)} for ${url}`)
  }

  stats.lastSteamRequest = Date.now()
  stats.requestsInThisMinute++
  targetIP.lastRequestAt = Date.now()
  targetIP.lastStatusCode = json.statusCode || -1
  targetIP.totalRequestsThisMinute++

  return json
}

async function getCurrentIPPool() {
  try {
    const res = await fetch(`${process.env.FLUX_PROXY_GATEWAY}/proxies`)
    const json = await res.json()
    const serverIPs = Object.keys(json)
    let added = 0

    for (const ip of serverIPs) {
      //if (i > 10 || stats.knownIPs.length > 10) return
      if (added > 1 || stats.knownIPs.filter(o => o.lastStatusCode !== 429).length > 10) {
        continue
      }
      const existing = stats.knownIPs.find(o => o.ip === ip)
      if (existing) {
        continue
      }

      const newIP = {
        ip,
        nextRequestAt: Date.now(),
        requestsSinceLastRatelimit: 0,
        ratelimits: [],
        lastStatusCode: 0,
        lastRequestAt: 0,
        lastSuccessfulRequest: 0,
        successfulRequestsThisMinute: 0,
        totalRequestsThisMinute: 0,
        totalPerMinute: {},
        successesPerMinute: {},
        requests: [],
        lastRateLimitAt: 0
      }

      const cookie = await getSessionCookies(ip)
      newIP.cookieString = cookie
      stats.knownIPs.push(newIP)
      added++
    }

    stats.poolSize = Object.keys(json).length
  } catch (e) {
    stats.poolSize = 0
  }
  return stats.poolSize
}

function getUsableProxies() {
  return stats.knownIPs.filter(o => o.nextRequestAt < Date.now() + TIME_BETWEEN_REQUEST && o.lastStatusCode !== 429).length
}

function maxRequestsPerMinute() {
  return getUsableProxies() * (60000 / TIME_BETWEEN_REQUEST)
}

async function getSessionCookies(ip) {
  const body = {
    method: 'GET',
    url: "https://steamcommunity.com/",
    headers: {
      "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8",
      "Accept-Encoding": "gzip, deflate, br",
      "Accept-Language": "en-GB,en;q=0.7",
      "Cache-Control": "max-age=0",
      "Connection": "keep-alive",
      "Host": "steamcommunity.com",
      "Sec-Fetch-Dest": "document",
      "Sec-Fetch-Mode": "navigate",
      "Sec-Fetch-Site": "none",
      "Sec-Fetch-User": "?1",
      "Sec-GPC": "1",
      "Upgrade-Insecure-Requests": "1",
      "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36"
    },
    timeout: 60000,
    maxRedirects: 1,
    successCodes: [200, 429, 500],
    ip
  }

  let json

  log.info(`Getting sessionid for ${ip}`)
  const res = await fetch(`${process.env.FLUX_PROXY_GATEWAY}/request`, {
    method: 'post',
    body: JSON.stringify(body),
    headers: {
      'Content-Type': 'application/json'
    }
  })

  json = await res.json()

  if (!json?.headers?.['set-cookie']) {
    console.error(`Could not find sessionid cookie for ${ip}`)
    return undefined
  }

  const sessionidCookie = json.headers['set-cookie'].find(o => o.startsWith('sessionid'))
  const steamCountryCookie = json.headers['set-cookie'].find(o => o.startsWith('steamCountry'))

  const sessionid = sessionidCookie.match(/sessionid=[a-zA-Z0-9]+;/)?.[0]
  const steamCountry = steamCountryCookie.match(/steamCountry=[^;]+;/)?.[0]

  return `${sessionid} ${steamCountry} timezoneOffset=0,0`
}

module.exports = {
  makeSteamRequest,
  stats,
  getCurrentIpPool: () => stats.poolSize,
  usableProxies: getUsableProxies,
  avgUsableProxies: getUsableProxies,
  maxRequestsPerMinute
}