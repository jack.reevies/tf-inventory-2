import { wait } from "./helpers"
import { randomUUID } from "crypto"

export const Queue = (name, maxConcurrent = 5, startImmediately = true) => {
  const queue = [] // [string, () => Promise<any>][]
  const active = {} // Record<string, [string, () => Promise<any>]>
  const token = { pause: false }

  function addToQueue(name, promise) {
    queue.push([name, promise])
  }

  async function startQueue() {
    while (true) {
      while (token.pause) {
        await wait(100)
      }

      if (Object.values(active).length >= maxConcurrent || !queue.length) {
        await wait(100)
        continue
      }

      const item = queue.splice(0, 1)[0]
      const nonce = randomUUID()

      active[nonce] = item

      // console.log(`[Queue: ${name}] Queue Size: ${getQueueLength()} || Active Tasks: ${getActiveLength()} || Task: ${nonce}`)
      item[1]().finally(() => {
        delete active[nonce]
      })
    }
  }

  function getQueueLength() {
    return queue.length
  }

  function getActiveLength() {
    return Object.values(active).length
  }

  function pauseQueue() {
    token.pause = true
  }

  function resumeQueue() {
    token.pause = false
  }

  function isInQueue(name) {
    return !!(queue.find(o => o[0] === name) || Object.values(active).find(o => o[0] === name))
  }

  function isQueueFull() {
    return maxConcurrent <= getQueueLength() + getActiveLength()
  }

  if (startImmediately) {
    startQueue()
  }

  return {
    addToQueue,
    startQueue,
    getQueueLength,
    getActiveLength,
    pauseQueue,
    resumeQueue,
    isInQueue,
    isQueueFull
  }
}