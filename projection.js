const MongoInventory = require('./dbApiTests/mInventory')
const MongoInstanceItem = require('./dbApiTests/mInstanceItem')
const SpecificId = require('@spacepumpkin/tf-specific-id')
const MongoProjectionChangeSet = require('./dbApiTests/mProjectionChangeSet')
const MongoGCInventory = require('./dbApiTests/mGCInventory')
const log = require('pino')({ prettyPrint: { colorize: true } })
const Mongo = require('./mongo')
const { getDropWeek } = require('./helpers')
const mInstanceItemCreator = require('./dbApiTests/mInstanceItemCreator')

async function findInventory(steamid64) {
  if (!steamid64) {
    const error = new Error('No steamid64 was specified')
    error.code = 400
    throw error
  }

  const inv = await MongoInventory.findOne({ steamid64 }).lean().exec()
  if (!inv) {
    const error = new Error('Could not find an inventory belonging to that steamid64')
    error.code = 400
    throw error
  }
  return inv
}

async function tempCreateGCInventory(steamid64, items) {
  try {
    const inventory = { steamid64, date: Date.now(), items: {} }

    for (const item of items) {
      const validationError = validateHasKeys(item, ['original_id', 'id', 'defindex', 'quality'])
      if (validationError) {
        continue
      }

      const result = SpecificId.getFromRawDetails(null, item.defindex, item.quality)

      if (typeof (result) === 'object') {
        inventory.items[item.id] = result.getRawString()
      }
    }

    await MongoGCInventory.updateOne({ steamid64 }, inventory, { upsert: true }).exec()
  } catch (e) {
    log.error(`Failed to create GCInventory because ${e.stack}`)
  }
}

async function createFromGC(steamid64, items) {
  tempCreateGCInventory(steamid64, items)
  const inv = await findInventory(steamid64)
  const mInstanceItems = await MongoInstanceItem.find({ $or: items.map(item => { return { id: item.id } }) }).lean().exec()
  const newInstanceItems = []
  const newCreators = []

  const addedItems = []
  const removedItems = []

  for (let i = 0; i < items.length; i++) {
    const item = items[i]

    const validationError = validateHasKeys(item, ['original_id', 'id', 'defindex', 'quality'])
    if (validationError) {
      continue
    }

    const mInstanceItem = mInstanceItems.find(instanceItem => instanceItem.id === item.id)
    const invItem = inv.items[item.id]

    if (!mInstanceItem || !mInstanceItem.id) {
      const result = SpecificId.getFromRawDetails(null, item.defindex, item.quality)
      if (typeof (result) === 'object') {
        newInstanceItems.push({
          id: invItem.id,
          specific_id: result.getRawString(),
          steamid64,
          $setOnInsert: { time: Date.now() }
        })
        newCreators.push({
          id: invItem.id,
          steamid64,
          $setOnInsert: { time: Date.now() },
          week: getDropWeek(Date.now())
        })
      } else {
        log.info(`Skipping adding ${item.id} as it's probably not valid given only defindex and quality`)
        continue
      }
    }

    if (!invItem) {
      // This must be a new item
      addedItems.push(item.id)
    }
  }

  // Now find what items exist in inventory but not GC inventory

  for (let i = 0; i < inv.items.length; i++) {
    const invItem = inv.items[i]
    const gcItem = items.find(gcItem => gcItem.id === invItem)
    if (!gcItem) {
      // We must assume that the item is no longer in the inventory according to GC
      removedItems.push(invItem)
    }
  }

  if (newInstanceItems.length > 0) {
    Mongo.saveBulk(newInstanceItems, 'id', MongoInstanceItem)
    Mongo.saveBulk(newCreators, 'id', mInstanceItemCreator)
  }

  if (addedItems.length > 0 || removedItems.length > 0) {
    await MongoProjectionChangeSet.create([{
      new_items: addedItems,
      removed_items: removedItems,
      time: Date.now(),
      fromGC: true,
      last_time: inv.date,
      steamid64
    }])
  }
}

async function recordManualChanges(steamid64, added, removed) {
  const inv = await findInventory(steamid64)

  /** @type {[{id, original_id, defindex, quality}]} */
  const jsonNewItems = added || []
  /** @type {[{id, original_id}]} */
  const jsonRemovedItems = removed || []

  const addedItems = []
  const addedItemsCreators = []
  const removedItems = []

  for (let i = 0; i < jsonRemovedItems.length; i++) {
    const removedItem = jsonRemovedItems[i]

    const validationError = validateHasKeys(removedItem, ['original_id', 'id'])
    if (validationError) {
      continue
    }

    const index = removedItems.findIndex(item => item === removedItem)

    if (index === -1) {
      removedItems.push(removedItem)
    }
  }

  for (let i = 0; i < jsonNewItems.length; i++) {
    const newItem = jsonNewItems[i]

    const validationError = validateHasKeys(newItem, ['original_id', 'id', 'defindex', 'quality'])
    if (validationError) {
      continue
    }

    const index = addedItems.findIndex(item => item === newItem)

    if (index === -1) {
      const result = SpecificId.getFromRawDetails(null, newItem.defindex, newItem.quality)
      if (typeof (result) === 'object') {
        addedItems.push({
          id: newItem.id,
          specific_id: result.getRawString(),
          steamid64,
          $setOnInsert: { time: Date.now() }
        })
        addedItemsCreators.push({
          id: newItem.id,
          steamid64,
          $setOnInsert: { time: Date.now() },
          week: getDropWeek(Date.now())
        })
      } else {
        log.info(`Skipping adding ${newItem.id} as it's probably not valid given only defindex and quality`)
      }
    }
  }

  Mongo.saveBulk(addedItems, 'id', MongoInstanceItem)
  Mongo.saveBulk(addedItemsCreators, 'id', mInstanceItemCreator)

  if (addedItems.length > 0 || removedItems.length > 0) {
    await MongoProjectionChangeSet.create([{
      new_items: addedItems.map(item => item.id),
      removed_items: removedItems.map(item => item.id),
      time: Date.now(),
      last_time: inv.date,
      steamid64
    }])
  }
}

function validateHasKeys(item, keys) {
  for (let i = 0; i < keys.length; i++) {
    const key = keys[i]
    if (!item[key]) {
      return `Item does not have ${key} key`
    }
  }
}

module.exports = {
  createFromGC,
  recordManualChanges
}
