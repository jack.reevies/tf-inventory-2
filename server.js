const polka = require('polka')
const { json } = require('body-parser')
const cors = require('cors')
const compression = require('compression')
const PORT = process.env.PORT || 8080
const pjson = require('./package.json')
const log = require('pino')({ prettyPrint: { colorize: true, translateTime: true } })
const SERVER_START_TIME = Date.now()

const Inventory = require('./inventory')
const Projection = require('./projection')
const Droplogger = require('./droplogger')
const Migrations = require('./migrations')
const { convertToTimestamp, getDropWeek } = require('./helpers')
const { sayUnhandledRejection, sayUnhandledException } = require('./discord')
const { SteamRequest } = require('./inventory')
const mIdlerTimes = require('./dbApiTests/mIdlerTimes')
const { asyncCursor, saveBulk } = require('./mongo')
const MIN_WISP_DATE = 200722
const MIN_WISP_SUBVER = 5
let totalTimeLagged = 0
let gotItemSchema = false
let doneMigrations = false

if (!('toJSON' in Error.prototype)) {
  Object.defineProperty(Error.prototype, 'toJSON', {
    value: function () {
      var alt = {}

      Object.getOwnPropertyNames(this).forEach(function (key) {
        if (key === 'stack') return
        alt[key] = this[key]
      }, this)

      return alt
    },
    configurable: true,
    writable: true
  })
}

log.info(`Started: version ${pjson.version}`)

process.on('uncaughtException', err => {
  log.error(err)
  if (err.messaage.indexOf("TypeError [ERR_INVALID_ARG_TYPE]: The \"chunk\" argument must be one of type string or Buffer. Received type object") === -1) {
    sayUnhandledException(err)
  }
  setTimeout(() => {
    process.exit(1)
  }, 5000)
})

process.on('unhandledRejection', (reason, promise) => {
  log.error(reason)
  sayUnhandledRejection(reason)
  setTimeout(() => {
    process.exit(1)
  }, 5000)
})

function apiRoot(req, res) {
  const mem = process.memoryUsage()
  const heap = Math.round((mem.heapUsed / 1024 / 1024) * 100) / 100
  const total = Math.round((mem.heapTotal / 1024 / 1024) * 100) / 100
  const rss = Math.round((mem.rss / 1024 / 1024) * 100) / 100
  const ext = Math.round((mem.external / 1024 / 1024) * 100) / 100

  const stats = Inventory.getStats()

  const full = {
    up_time: convertToTimestamp(Date.now() - SERVER_START_TIME),
    service: pjson.name,
    version: pjson.version,
    wiki: pjson.homepage,
    patch_notes: pjson.patch_notes,
    debug: {
      timeLostToLagMs: totalTimeLagged,
      uptimeLost: totalTimeLagged / (Date.now() - SERVER_START_TIME)
    },
    droplog_stats: Droplogger.stats,
    stats,
    memory: {
      heap_used: `${heap} MB`,
      heap_total: `${total} MB`,
      rss: `${rss} MB`,
      external: `${ext} MB`
    }
  }
  sendJson(200, full, res)
}

async function getInventory(req, res) {
  req.on('close', () => {
    if (!res.finished) {
      log.warn('client disconnected unexpectedly')
      req.closed = true
    }
  })

  // if (!Inventory.canWeHandleRequests()) {
  //   sendJson(503, { message: 'Queue is overloaded - please wait a bit' }, res)
  //   return
  // }

  if (!req.query.steamid) {
    sendJson(400, { message: "You didn't specific a steamid" }, res)
    return
  }

  if (!req.query.maxage || req.query.maxage === "0") {
    req.query.maxage = -1
  } else if (req.query.maxage) {
    req.query.maxage = parseInt(req.query.maxage)
    if (isNaN(req.query.maxage)) {
      sendJson(400, { message: 'maxage param was not a valid number' })
    }
  }

  let data = await Inventory.getInventory(req.query.steamid, Number.MAX_SAFE_INTEGER, req.query.timeout, req.query.allow_projection, req.query.maxattempts || 5)
  //let data = await Inventory.getInventory(req.query.steamid, req.query.maxage || -1, req.query.timeout, req.query.allow_projection, req.query.maxattempts || 5)

  if (!data) {
    if (!req.closed) {
      sendJson(500, data, res)
      return
    }
  }
  if (req.query.pure || req.query.nostatus) {
    data = data.inventory
  }

  if (!req.closed) {
    sendJson(200, data, res)
  }
}

async function getDropsForSession(req, res) {
  const sessionAlias = req.query.alias
  if (!sessionAlias) {
    sendJson(400, { message: 'Expected a "alias" query param' }, res)
    return
  }

  const allDrops = await Droplogger.getDropsForSession(sessionAlias)

  sendJson(200, allDrops, res)
}

async function getWeeklyDropsForDevice2(req, res) {
  const week = Number(req.query.week) || getDropWeek(Date.now())
  const device = req.query.alias
  if (!device) {
    sendJson(400, { message: 'Expected a "alias" query param' }, res)
    return
  }
  const source = req.query.source
  if (!source) {
    sendJson(400, { message: 'Expected a "source" query param' }, res)
    return
  }

  const deviceDrops = await Droplogger.getDeviceDropsForWeek(device, week, source)

  sendJson(200, deviceDrops, res)
}

async function getDrops(req, res) {
  if (!req.query.steamid) {
    sendJson(400, new Error("You didn't specific a steamid"), res)
    return
  }

  const start = Number(req.query.start || 0)
  const end = Number(req.query.end || 0)
  const data = await Droplogger.getDropsTimeSpan(req.query.steamid, start, end)
  data.new = Object.keys(data.new).map(key => { return { ...data.new[key], id: key } })
  data.removed = Object.keys(data.removed).map(key => { return { ...data.removed[key], id: key } })

  sendJson(200, data, res)
}

async function getItems(req, res) {
  if (!req.body || Object.keys(req.body).length === 0) {
    sendJson(400, new Error('Expected an array of ids or an object with ids as keys'), res)
    return
  }

  let input = {}
  if (Array.isArray(req.body)) {
    req.body.forEach(item => {
      input[item] = ''
    })
  } else {
    input = req.body
  }
  const output = await Inventory.getItems(input)
  sendJson(200, output, res)
}

async function gcUpdate(req, res) {
  try {
    await Projection.createFromGC(req.body.steamid64, req.body.items)
    sendJson(204, {}, res)
  } catch (e) {
    sendJson(500, e.message, res)
  }
}

async function manualChanges(req, res) {
  try {
    await Projection.recordManualChanges(req.body.steamid64, req.body.new_items, req.body.removed_items)
    sendJson(204, {}, res)
  } catch (e) {
    sendJson(500, e.message, res)
  }
}

async function pricesFromClassInstance(req, res) {
  try {
    if (SteamRequest.usableProxies() === 0) {
      sendJson(503, { message: 'We are currently ratelimitred on Steam - please try later', res })
      return
    }
    const results = await Inventory.getPricesFromEscrowData(req.body)
    sendJson(200, results, res)
  } catch (e) {
    sendJson(500, e.message, res)
  }
}

async function watchInventories(req, res) {
  const { steamids, hours } = req.body
  Droplogger.watchInventoriesFor(steamids, hours)
  sendJson(204, null, res)
}

function associateIdlersWithDevice(req, res) {
  const { alias, steamid64s } = req.body
  if (!alias) {
    sendJson(400, { message: 'No alias given (who are you?)' }, res)
    log.warn('Responding 400 for associateIdlersWithDevice because no alias was given')
    return
  }
  if (!steamid64s || steamid64s.length === 0 || !Object.keys(steamid64s).length) {
    sendJson(400, { message: 'Expected steamid64s to not be empty' }, res)
    log.warn('Responding 400 for associateIdlersWithDevice because steamid64s was not present')
    return
  }
  Droplogger.associateIdlersWithDevice(alias, steamid64s)
  sendJson(204, null, res)
}

async function recordIdlerTimes(req, res) {
  // body is an object of keys of steamids with values of minutes to add on

  const start = Date.now()
  let week = 0

  if (req.query.week) {
    week = Number(req.query.week)
  }

  if (!week) {
    week = getDropWeek(Date.now())
  }

  if (req.query.total) {
    await saveBulk(Object.keys(req.body).map(o => { return { week, steamid64: o, minutes: req.body[o] } }), ['steamid64', 'week'], mIdlerTimes)
  } else {
    await saveBulk(Object.keys(req.body).map(o => { return { week, steamid64: o, $inc: { minutes: req.body[o] || 1 } } }), ['steamid64', 'week'], mIdlerTimes)
  }

  log.info(`Took ${Date.now() - start}ms to update idler times`)
  sendJson(200, {}, res)
}

async function getDeviceDropsOverview(req, res) {
  try {
    sendJson(200, await Droplogger.getLeaderboardOverview(), res)
  } catch (e) {
    sendJson(500, e, res)
  }
}

function start() {
  polka()
    .use(cors())
    .use(json({ limit: '10mb' }))
    .use(compression({ filter: req => true }))
    .use((req, res, next) => { log.info(`Request at ${req.method} ${req.url} from ${req.socket.remoteAddress}`); next() })
    .use(isServerReady)
    // .use((req, res) => {
    //   sendJson(503, { message: 'currently debugging in live - because all good devs do (tm)' }, res)
    // })
    .get('/', apiRoot)
    .get('/inventory', getInventory)
    .get('/drops', getDrops)
    .get('/drops/sessions', getDropsForSession)
    .get('/drops/devices', getWeeklyDropsForDevice2)
    .get('/drops/devices/overview', getDeviceDropsOverview)
    .post('/idlers', recordIdlerTimes)
    .post('/watch', watchInventories)
    .post('/gc', verifyWispVersion, gcUpdate)
    .post('/manual', verifyWispVersion, manualChanges)
    .post('/items', verifyWispVersion, getItems)
    .post('/pricesFromClassInstance', pricesFromClassInstance)
    .post('/device/idlers', associateIdlersWithDevice)
    .listen(PORT)

  let lagTime = 0
  setInterval(() => {
    const laggedFor = Date.now() - lagTime
    if (lagTime && laggedFor > 2000) {
      totalTimeLagged += 2000
      log.warn(`Lag Detected for ${laggedFor}ms`)
    }
    lagTime = Date.now()
  }, 1000)

  const startMigrations = async () => {
    gotItemSchema = true
    require('./experiments')
    await Migrations.doMigrations()
    Inventory.ItemSchema.events.removeListener('schema', startMigrations)
  }

  Inventory.ItemSchema.events.on('schema', startMigrations)

  Migrations.events.on('complete', () => {
    doneMigrations = true
    Droplogger.start()
  })
}

function sendJson(statusCode, body, res) {
  res.statusCode = statusCode
  res.setHeader('content-type', 'application/json')
  res.end(JSON.stringify(body))
}

function verifyWispVersion(req, res, next) {
  const ver = req.headers['x-wisp-version']
  if (ver) {
    const major = ver.substring(0, ver.indexOf('.'))
    const date = ver.substring(major.length + 1, major.length + 7)
    const subver = ver.substring(major.length + 8)
    const numDate = Number(date)
    const numSub = Number(subver)
    if (numDate > MIN_WISP_DATE || (numDate === MIN_WISP_DATE && numSub >= MIN_WISP_SUBVER)) {
      next()
      return
    }
  }
  sendJson(400, { message: 'outdated!' }, res)
}

function isServerReady(req, res, next) {
  if (gotItemSchema && doneMigrations) {
    next()
  } else {
    res.setHeader('Retry-After', '30')
    sendJson(503, {
      uptime: convertToTimestamp(Date.now() - SERVER_START_TIME),
      status: 'INITIALIZING',
      service: pjson.name,
      version: pjson.version
    }, res)
    if (Date.now() - (1000 * 60 * 5) > SERVER_START_TIME) {
      process.exit(1)
    }
  }
}

start()
