const INVMAN_CHANNEL = { channel: '739887101742678106' }
const { getPublicIp, convertToTimestamp } = require('./helpers')
const { post, CONTENT_TYPES } = require('@spacepumpkin/minimal-request')
const packageJson = require('./package.json')
const { usableProxies } = require('./steamRequest')

const THUMBNAILS = {
  ALIVE_HEART: 'https://cdn1.iconfinder.com/data/icons/charity-7/300/heartbeat-life-landscape-plant-flower-alive-sport-fitness-512.png',
  GLITCH_SKULL: 'https://i.imgur.com/zcDEHL1.png',
  BACKPACK: 'https://wiki.teamfortress.com/w/images/thumb/e/e8/Backpack_case.png/200px-Backpack_case.png',
  RED_WARNING: 'https://www.pngitem.com/pimgs/m/119-1190874_warning-icon-png-png-download-icon-transparent-png.png',
  YELLOW_WARNING: 'https://www.clipartmax.com/png/middle/206-2068155_warning-sign-emoji-caution-emoji-png.png',
  DISCONNECT: 'https://i.imgur.com/lYkKtD2.png'
}

const TYPES = {
  EXCEPTION: { title: 'Uncaught Exception', interval: 1000 },
  REJECTION: { title: 'Uncaught Rejection', interval: 1000 },
  DEGRADED_POOL: { title: 'Degraded Proxy Pool', interval: 1000 * 60 * 2 },
  INVENTORY_TROUBLE: { title: 'Trouble fetching inventories', interval: 1000 * 60 * 2 },
  QUEUE_OVERLOADED: { title: 'Queue Overloaded!', interval: 1000 * 60 * 2 },
  DROPLOGGER_DEAD: { title: 'Droplogger might be dead', interval: 1000 * 60 * 10 },
  REFRESH_PROXY: { title: 'Requested Proxy IP Refresh', interval: 1000 * 60 },
  ALIVE: { title: 'I am alive!', interval: 0 }
}

let myIp = null
const footer = { text: 'InvMan@UNKNOWN', url: THUMBNAILS.BACKPACK }

module.exports.sayIAmAlive = function iAmAlive(ip) {
  postDiscordMessage(
    TYPES.ALIVE,
    { 'Current IP': ip, Version: packageJson.version },
    INVMAN_CHANNEL,
    THUMBNAILS.ALIVE_HEART
  )
}

module.exports.sayUnhandledException = function unhandledException(err) {
  postDiscordMessage(TYPES.EXCEPTION, {
    message: err.stack.substring(0, 2000)
  }, INVMAN_CHANNEL, THUMBNAILS.GLITCH_SKULL)
}

module.exports.sayUnhandledRejection = function unhandledRejection(err) {
  postDiscordMessage(TYPES.REJECTION, {
    message: typeof (err) === 'string' ? err.substring(0, 2000) : err.stack.substring(0, 2000)
  }, INVMAN_CHANNEL, THUMBNAILS.GLITCH_SKULL)
}

/**
 *
 */
module.exports.sayHerokuProxyUnreachable = function herokuProxyUnreachable(poolSize, expected, deadUrls, goodUrls) {
  postDiscordMessage(TYPES.DEGRADED_POOL, {
    'IP Pool Size': `${poolSize} / ${expected}`,
    'Unusable Proxies': deadUrls.join('\r\n'),
    'Good Proxies': goodUrls.join('\r\n')
  }, INVMAN_CHANNEL, THUMBNAILS.DISCONNECT)
}

module.exports.sayInventoryTrouble = function inventoryTrouble(queueSize, lastSuccessRequestDate) {
  postDiscordMessage(TYPES.INVENTORY_TROUBLE, {
    'Current Queue Size': queueSize,
    'Last Successful Fetch': lastSuccessRequestDate
  }, INVMAN_CHANNEL, THUMBNAILS.RED_WARNING)
}

module.exports.sayQueueOverloaded = function queueOverloaded(queueSize, lastSuccessRequestDate, requestsThisMinute, requestsLastMinute, usableProxies, poolSize, avgWaitTime) {
  postDiscordMessage(TYPES.QUEUE_OVERLOADED, {
    'Current Queue Size': queueSize.toString(),
    'Requests': `${requestsThisMinute} (vs ${requestsLastMinute} last minute)`,
    'Usable Proxies': `${usableProxies} (out of ${poolSize})`,
    'Avg Wait Time': convertToTimestamp(avgWaitTime),
    'Last Successful Fetch': lastSuccessRequestDate
  }, INVMAN_CHANNEL, THUMBNAILS.YELLOW_WARNING)
}

module.exports.sayDroploggerDead = function droploggerDead(lastAction, queueSize) {
  postDiscordMessage(TYPES.DROPLOGGER_DEAD,
    { 'Last Action': lastAction, "Queue Size": queueSize },
    INVMAN_CHANNEL,
    THUMBNAILS.RED_WARNING
  )
}

module.exports.sayRefreshProxy = function refreshProxy(appName, appUrl, status) {
  postDiscordMessage(TYPES.REFRESH_PROXY,
    { 'App Name': appName, Url: appUrl, 'Proxy Manager Response': status },
    INVMAN_CHANNEL,
    THUMBNAILS.RED_WARNING
  )
}

async function postDiscordMessage(type, content, discord, thumbnail) {
  if (type.last && type.last > Date.now() - type.interval) { return }
  type.last = Date.now()
  await post(`http://${process.env.ALTEREGO_URL || '188.166.151.125:9117'}/custom/new`, {
    contentType: CONTENT_TYPES.JSON,
    body: {
      sender: 'InvMan',
      discord,
      title: type.title,
      content,
      footer,
      thumbnail
    }
  })
}
async function setIP() {
  const ip = await getPublicIp()
  if (ip.length > 30) {
    return setIP()
  } else {
    myIp = ip
    footer.text = `InvMan@${ip}-flux`
    module.exports.sayIAmAlive(myIp)
  }
}

setIP()
