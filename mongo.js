const log = require('pino')({ prettyPrint: { colorize: true } })

function saveBulk (documents, filterNames, model, insertOnly) {
  return new Promise(function (resolve, reject) {
    if (!documents || !documents.length) {
      resolve()
      return
    }
    let awaiting = 0
    function bulkUpdateCallback (err, r) {
      if (err) {
        log.error(err)
      }
      awaiting--
      if (awaiting <= 0) {
        resolve()
      }
    }

    let bulkOps = []

    for (let i = 0; i < documents.length; i++) {
      const document = documents[i]
      let save = false
      if (document) {
        const unset = document.$unset
        delete document.$unset
        const setOnInsert = document.$setOnInsert
        delete document.$setOnInsert
        const min = document.$min
        delete document.$min
        const inc = document.$inc
        delete document.$inc
        if (insertOnly) {
          bulkOps.push({
            insertOne: document
          })
        } else {
          bulkOps.push({
            updateOne: {
              filter: {},
              update: { $set: document },
              upsert: true
            }
          })
          if (!Array.isArray(filterNames)) {
            filterNames = [filterNames]
          }
          filterNames.forEach(o => { bulkOps[bulkOps.length - 1].updateOne.filter[o] = document[o] })
          if (unset) {
            bulkOps[bulkOps.length - 1].updateOne.update.$unset = unset
          }
          if (setOnInsert) {
            bulkOps[bulkOps.length - 1].updateOne.update.$setOnInsert = setOnInsert
          }
          if (min) {
            bulkOps[bulkOps.length - 1].updateOne.update.$min = min
          }
          if (inc) {
            bulkOps[bulkOps.length - 1].updateOne.update.$inc = inc
          }
        }

        if (bulkOps.length % 1000 === 0) {
          const maxSize = 1024 * 1024 * 2
          const jsonSize = JSON.stringify(bulkOps).length

          if (jsonSize > maxSize) {
            save = true
            log.debug(`Pushing to save at length ${bulkOps.length} as this is over 2MB worth of instructions`)
          }
        }
      }

      if (save || i === documents.length - 1) {
        awaiting++
        model.collection.bulkWrite(
          bulkOps,
          { ordered: true, w: 1, upsert: true },
          bulkUpdateCallback
        )
        bulkOps = []
      }
    }
  })
}

async function asyncCursor (modelName, query, eachAsyncFn, options, returnOnReject) {
  try {
    await new Promise((resolve, reject) => {
      const cursor = query.cursor()
      cursor.addCursorFlag('noCursorTimeout', true)
      cursor.eachAsync(eachAsyncFn, options, err => {
        try { cursor.close() } catch (e) { log.error(`Couldnt close cursor because ${e.stack}`) }
        if (err) {
          log.error(`got an error inside eachAsync on asyncCursor for ${modelName}`)
          if (returnOnReject) {
            resolve()
            return
          }
          reject(err)
          return
        }
        resolve()
      })
    })
  } catch (e) {
    log.error(`caught outside eachAsync on asyncCursor for ${modelName}`)
    log.error(e)
    if (returnOnReject) {
      return
    }
    throw e
  }
}

module.exports = {
  saveBulk,
  asyncCursor
}
