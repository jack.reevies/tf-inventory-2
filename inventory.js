const SpecificId = require('@spacepumpkin/tf-specific-id')
const ItemSchema = require('@spacepumpkin/tf-itemschema')
const TF2Items = require('@spacepumpkin/tf2-items')
const log = require('pino')({ prettyPrint: { colorize: true, translateTime: true } })

const SteamRequest = require('./steamRequest')
const MongoInstanceItem = require('./dbApiTests/mInstanceItem')
const MongoSchemaItem = require('./dbApiTests/mSchemaItem')
const MongoPrice = require('./dbApiTests/mPrice')
const MongoProjectionChangeSet = require('./dbApiTests/mProjectionChangeSet')
const MongoInventory = require('./dbApiTests/mInventory')
const MongoGCInventory = require('./dbApiTests/mGCInventory')
const mDropSet = require('./dbApiTests/mDropSets')
const mSpecificIdItem = require('./dbApiTests/mSpecificIdItem')
const Mongo = require('./mongo')
const steamApiKeys = require('./steamApiKeys')
const { makeSteamRequest } = require('./steamRequest')
const { sayInventoryTrouble, sayQueueOverloaded } = require('./discord')
const { getDropWeek, waitForSchema, wait } = require('./helpers')
const { attachUrlsForSpecificIds, attachPricesToItems, attachSchemaInfoToItems } = require('./items')
const { asyncCursor } = require('./mongo')
const mInstanceItemCreator = require('./dbApiTests/mInstanceItemCreator')
const POST_FUCKERY_EPOCH = 1595189044000
const JOB_TIMEOUT = 1000 * 60 * 10
const events = new (require('events').EventEmitter)()
const MINIMUM_CACHE_FREQUENCY = 10

const stats = {
  debugInventoryParts: {}, //Record<parts: number, timeTaken: number[]>
  activeJobs: {},
  waitingSids: {},
  backlogged: true,
  spareSeconds: 0,
  lastAvgWaitTime: 0,
  avgWaitTimes: {}, //Record<epochMinute, waitTime>
  finishedJobTimes: {} // Record<epochMinute, waitTime>
}

function recordJobWaitTime(added) {
  const epochMinute = getEpochMinute()
  if (!stats.finishedJobTimes[epochMinute]) {
    stats.finishedJobTimes[epochMinute] = []
  }
  stats.finishedJobTimes[epochMinute].push(Date.now() - added)
}

function getEpochMinute(time) {
  if (!time) {
    time = Date.now()
  }
  return Math.round((time / 1000) / 60)
}

setTimeout(async () => {
  while (true) {
    const date = new Date()
    if (date.getSeconds() < 3) {
      log.info(`Started calculateLastMinuteWaitTimes at ${new Date().toTimeString()}`)
      setInterval(calculateLastMinuteWaitTimes, 60 * 1000)
      return
    }
    await wait(100)
  }
}, 1)

function calculateLastMinuteWaitTimes() {
  const epochMinute = getEpochMinute() - 1
  const finishedJobs = stats.finishedJobTimes[epochMinute] || []
  //const awaitingJobs = Object.values(stats.awaitingJobs).map(o => Date.now() - o.added)
  const waitTimes = [...finishedJobs].filter(o => o > 100)
  const sumWaitTimes = waitTimes.reduce((acc, obj) => acc + obj, 0)
  const avgWaitTime = Math.round(sumWaitTimes / waitTimes.length)
  stats.avgWaitTimes[epochMinute] = avgWaitTime
  stats.lastAvgWaitTime = avgWaitTime
  log.info(`!! Avg wait time is currently ${Math.round(avgWaitTime)} ms (calculated from ${waitTimes.length} requests)`)

  const awaitingCount = Object.values(stats.activeJobs).length
  if (avgWaitTime > 1000 * 20 && awaitingCount > SteamRequest.avgUsableProxies()) {
    sayQueueOverloaded(
      awaitingCount,
      new Date(SteamRequest.stats.lastSuccessfulRequest).toString(),
      SteamRequest.stats.requestsInThisMinute,
      SteamRequest.stats.requestsInLastMinute,
      SteamRequest.avgUsableProxies(),
      SteamRequest.getCurrentIpPool(),
      avgWaitTime
    )
    lastDiscordMessage = Date.now()
  }
}

watchQueue()

async function watchQueue() {
  while (true) {
    stats.backlogged = true
    while (Object.keys(stats.activeJobs).length >= SteamRequest.getCurrentIpPool()) {
      while (Object.keys(stats.activeJobs).length >= SteamRequest.getCurrentIpPool()) {
        await wait(100)
      }
      await wait(1000)
    }
    stats.backlogged = Object.keys(stats.activeJobs).length >= SteamRequest.getCurrentIpPool()
    await wait(100)
  }
}

function cleanupOldJobs() {
  const keys = Object.keys(stats.activeJobs).length
  for (let i = 0; i < keys.length; i++) {
    const key = keys[i]
    const job = stats.activeJobs[key]
    if (job.added > Date.now() - JOB_TIMEOUT) { continue }
    recordJobWaitTime(job.added)
    delete stats.activeJobs[key]
    log.warn(`Removed expired job for ${job.steamid} (added ${Date.now() - job.added}ms ago)`)
  }
}

function cacheItemSchema(apiKey, timeout) {
  return new Promise((resolve, reject) => {
    SpecificId.setSteamApiKey(apiKey)
    ItemSchema.fetchItemSchema(true)
    ItemSchema.events.on('schema', cache => {
      log.info('Got itemschema')
      resolve(cache)
    })
  })
}

function resolveSpecificIds(assets, descriptions, isInventory) {
  const tor = []
  let missingDescriptions = 0
  if (!assets) return []
  for (let i = 0; i < assets.length; i++) {
    const asset = assets[i]
    const description = descriptions.find(o => ((asset.instanceid === '0' && !o.instanceid) || o.instanceid === asset.instanceid) && o.classid === asset.classid)
    if (!description) {
      // console.log(`Couldnt find match for ${asset.assetid}`)
      missingDescriptions++
      continue
    }
    const quality = Array.isArray(description.tags) ? description.tags.find(o => o.category === 'Quality') : Object.values(description.tags).find(o => o.category === 'Quality')
    const isUnusual = quality ? typeof (quality) === 'object' ? quality.internal_name === 'rarity4' : description.tags[quality].internal_name === 'rarity4' : false // Unusual

    const UNCRAFTABLE_TAG = 'Not Usable in Crafting'
    const UNTRADABLE_TAG = 'Not Tradable'
    // const UNCRAFTABLE_UNTRADABLE_TAG = 'Not Tradable or Marketable'

    let isCraftable = true
    let isTradable = true
    if (description.descriptions) {
      const descriptions = description.descriptions
      const tags = Array.isArray(descriptions) ? descriptions.map(o => o.value) : Object.values(descriptions).map(o => o.value)
      isCraftable = !tags.find(o => o.indexOf(UNCRAFTABLE_TAG) > -1)
      isTradable = !tags.find(o => o.indexOf(UNTRADABLE_TAG) > -1)
      // if (tags.find(o => o.indexOf(UNCRAFTABLE_UNTRADABLE_TAG) > -1)) {
      //   isCraftable = isTradable = false
      // }
    }

    let containsItemName = null
    if (description.market_hash_name === 'Delivered Giftapult Package') {
      containsItemName = description.descriptions ? (Array.isArray(description.descriptions) ? description.descriptions.find(o => o.value.startsWith('\nContains:')) : Object.keys(description.descriptions).find(key => description.descriptions[key].value.startsWith('\nContains:'))) : null
      containsItemName = containsItemName.value.replace('\nContains: ', '')
    }

    let unusualId = null
    if (isUnusual) {
      const unusualString = description.descriptions ? description.descriptions.find(o => o.value.indexOf('Unusual Effect:') > -1) : null
      if (unusualString) {
        const unusualName = unusualString.value.replace('★ Unusual Effect: ', '').replace('Unusual Effect: ', '')
        unusualId = TF2Items.getUnusualIdFromEffectName(unusualName)
        if (!unusualId) {
          console.log(`Couldnt find an unusual effect id for ${unusualName}`)
        }
      }
    }

    const id = SpecificId.getFromMarketHashName(description.market_hash_name.replace('é', 'e'), isCraftable, unusualId, isTradable)

    if (containsItemName) {
      containsItemName = containsItemName.replace(/\n/g, ' ')
      if (containsItemName === 'Chemistry Set Series #2') {
        id.setTargetDefindex(20005)
      } else {
        const containsId = SpecificId.getFromMarketHashName(containsItemName, true)
        const containedItemDefindex = containsId.getDefindex()
        if (containedItemDefindex) {
          id.setTargetDefindex(containedItemDefindex)
        } else {
          containsItemName = `${containsItemName} (Factory New)`
          const containsId = SpecificId.getFromMarketHashName(containsItemName, true)
          const containedItemDefindex = containsId.getDefindex()
          if (containedItemDefindex) {
            id.setTargetDefindex(containedItemDefindex)
          } else {
            console.log(`Couldn't find defindex for the giftapult packages' contained item (${containsItemName})`)
            continue
          }
        }
      }
    }

    const validateResult = id.validate(true)
    if (validateResult.reason) {
      console.log(`Failed to get SpecificId for ${description.market_hash_name} because ${validateResult.reason}`)
      continue
    }

    const newItem = {
      ...id.getObject(),
      id: asset.assetid,
      full_name: SpecificId.getNameFromSpecificId(id),
      url: `https://steamcommunity-a.akamaihd.net/economy/image/${description.icon_url}`
    }
    if (!isTradable) {
      newItem.untradable = true
    }
    tor.push(newItem)
  }
  if (missingDescriptions > 0) {
    log.warn(`Failed to match ${missingDescriptions} assets (${isInventory ? 'isInventory' : 'isEscrow'})`)
  }
  return tor
}

let lastDiscordMessage = 0
function areWeOkay() {
  if (lastDiscordMessage > Date.now() - (1000 * 60 * 2)) { return }
  const awaitingJobs = Object.keys(stats.activeJobs).length
  if (SteamRequest.stats.lastSuccessfulRequest && SteamRequest.stats.lastSuccessfulRequest < Date.now() - (1000 * 60 * 5) && SteamRequest.stats.lastSteamRequest > SteamRequest.stats.lastSuccessfulRequest) {
    sayInventoryTrouble(Object.keys(stats.activeJobs).length, new Date(SteamRequest.stats.lastSuccessfulRequest).toString())
    lastDiscordMessage = Date.now()
  }
}


function canWeHandleRequests() {
  if (SteamRequest.usableProxies() === 0 || Object.keys(stats.activeJobs).length > 2 * SteamRequest.getCurrentIpPool()) {
    // We are overloaded - lets deny this request
    return false
  }
  return true
}

function logQueueWaiting() {
  if (Object.keys(stats.waitingSids).length) {
    log.warn(`There are ${Object.keys(stats.waitingSids).length} waiting SIDs (ipPool: ${SteamRequest.stats.poolSize} || usable Proxies: ${SteamRequest.usableProxies()} || activeJobs: ${Object.keys(stats.activeJobs).length} || waitingSids: ${Object.keys(stats.waitingSids).length})`)
  }
}

/**
 *
 * @returns {{date: Number, steamid64: String, items: []}}
 */
async function getInventory(steamid, maxage = 0, timeout = 120, fallback = true, allowProjections = true, maxAttempts = 5) {
  const existingJob = Object.keys(stats.activeJobs).find(o => stats.activeJobs[o].steamid === steamid)

  if (existingJob) {
    const job = stats.activeJobs[existingJob]
    if (Date.now() - job.added > 1000 * 60 * 5) {
      log.info(`We already have a job for ${steamid} (added ${Date.now() - job.added}ms ago) but deleting and re-running because its really old`)
    }
    log.info(`We already have a job for ${steamid} (added ${Date.now() - job.added}ms ago) - Returning this instead`)
    if (job.result) {
      log.info('Chained request has already finished - returning its cache result')
      return job.result
    }
    return job.task
  }

  // if (SteamRequest.usableProxies() === 0) {
  //   return
  // }

  if (maxage < Number.MAX_SAFE_INTEGER) {
    let waitedFor = 0
    let totalWaitedFor = 0
    await wait(Math.random() * 100 + 10)
    while (SteamRequest.stats.poolSize <= Object.keys(stats.activeJobs).length) {
      const waitFor = Math.random() * 100 + 10
      await wait(waitFor)
      waitedFor += waitFor
      totalWaitedFor += waitFor
      stats.waitingSids[steamid] = true
      if (waitedFor > 10000) {
        waitedFor = 0
      }
    }

    delete stats.waitingSids[steamid]
  }

  const nonce = `${steamid}-${Math.floor(Math.random() * 10000)}`
  stats.activeJobs[nonce] = { steamid, added: Date.now(), task: null }

  await waitForSchema(ItemSchema)
  areWeOkay()

  stats.activeJobs[nonce].task = getInventoryJob(steamid, maxage, timeout, fallback, allowProjections, maxAttempts = 5)
  stats.activeJobs[nonce].task.then(inventory => {
    stats.activeJobs[nonce].result = inventory
    recordJobWaitTime(stats.activeJobs[nonce].added)
    delete stats.activeJobs[nonce]
  })

  stats.activeJobs[nonce].task.catch(e => {
    recordJobWaitTime(stats.activeJobs[nonce].added)
    log.error(`Job failed with ${e.stack}`)
    delete stats.activeJobs[nonce]
  })

  return stats.activeJobs[nonce].task
}

async function getInventoryJob(steamid, maxage, timeout, fallback, allowProjections, maxAttempts = 5) {
  let inventory = null
  let attemptsLeft = 3
  while (attemptsLeft > 0) {
    try {
      inventory = await getInventoryInner(steamid, maxage, timeout, fallback, allowProjections, maxAttempts)
      break
    } catch (e) {
      log.error(`Failed to get inventory for ${steamid} because ${e.message}`)
    }
    attemptsLeft--
    await wait(1000)
  }
  try {
    if (inventory.inventory) {
      inventory.inventory = compressInventory(inventory.inventory)
    }
  } catch (e) {
    log.error(`Failed to compress inventory for ${steamid} because ${e.message}`)
  }
  return inventory
}

/**
 *
 * @returns {{date: Number, steamid64: String, items: []}}
 */
async function getInventoryInner(steamid, maxage = 0, timeout = 120, fallback = true, allowProjections = true, attempt = 0, maxAttempts = 5) {
  const cachedInventory = await getRecentInventoryCache(steamid, maxage, allowProjections)
  const status = { source: 'Cache', steamid, maxCacheAge: maxage, steamTimeoutMs: timeout, returnCacheOnFail: fallback, allowProjections }
  let inventory = { parts: 0 }
  // TEMP CODE
  // cachedInventory.stale = true
  if (!cachedInventory || cachedInventory.stale) {
    // We either don't have the inventory cached or the cache is too old
    const start = Date.now()
    await getInventoryPart(steamid, null, inventory, timeout ? timeout * 1000 : null, 75)

    log.info(`Got response for ${steamid} from Steam in ${Date.now() - start} ms (${inventory.parts} parts) (${inventory.assets ? inventory.assets.length : 'no'} items) (requests this minute: ${SteamRequest.stats.requestsInThisMinute}) (queue: ${Object.keys(stats.activeJobs).length})`)

    if (!stats.debugInventoryParts[inventory.parts]) {
      stats.debugInventoryParts[inventory.parts] = []
    }

    stats.debugInventoryParts[inventory.parts].push(Date.now() - start)

    if (!inventory.assets || inventory.assets.length === 0) {
      const isPrivate = await isInventoryPrivate(steamid)
      if (isPrivate) {
        // Profile is private - we can't get inventory - we should return now
        log.info(`Requested inventory (${steamid}) is private - not storing cache`)
        inventory.isPrivate = true
        return { status: { ...status, source: 'Steam', statusCode: 403 }, inventory: { items: [], isPrivate: true } }
      }
    }

    if (!inventory || (!inventory.total_inventory_count && inventory.total_inventory_count !== 0)) {
      // We failed - we don't know how yet, we just did. hmmmmmm
      // We should return the stale cache if fallback is true
      if (fallback && cachedInventory) {
        return { status: { ...status, cacheAge: cachedInventory.cacheAge }, inventory: cachedInventory.inventory }
      }
      return { status: { ...status, statusCode: 500 } }
    }
    if (!inventory.assets && inventory.total_inventory_count > 0) {
      log.warn(`inventory.assets for ${steamid} is null. I don't know what to do`)
      return { status: { ...status, source: 'Steam', statusCode: 400 }, inventory: { items: [] } }
    }

    const items = await resolveSpecificIds(inventory.assets, inventory.descriptions, 1)
    updateKnownSpecificIds(items)
    inventory = { inventory: { steamid64: steamid, date: Date.now(), items } }
    if (cachedInventory) {
      if (cachedInventory.inventory.items.length !== 0 && inventory.inventory.items.length === 0 && attempt < 5) {
        // This looks sus - we should wait a few seconds and try the request again
        log.warn(`Inventory for ${steamid} looks sus - steam says no items but our last cache does. Regetting (attempt: ${attempt} / ${maxAttempts})`)
        return getInventoryInner(steamid, maxage, timeout, fallback, allowProjections, ++attempt, maxAttempts)
      }
    }
    saveInstanceItemsToMongo(steamid, inventory.inventory.items)
    saveInventoryToMongo(inventory.inventory)
    const { totalWorth, metalWorth } = await attachPricesToItems(inventory.inventory.items)
    attachSchemaInfoToItems(inventory.inventory.items)
    inventory.total_worth = totalWorth
    inventory.metal_worth = metalWorth
    events.emit('inventory', { inventory: inventory.inventory })
    log.info(`Got inventory for ${steamid} - with ${inventory.inventory?.items?.length} items`)
    return { status: { ...status, source: 'Steam', statusCode: 200 }, inventory: inventory.inventory }
  }

  // Else we have valid cache that we can return to user
  for (let i = 0; i < cachedInventory.inventory.items.length; i++) {
    const item = cachedInventory.inventory.items[i]
    const specificId = SpecificId.fromOwnId(item.specific_id)
    if (!specificId) {
      // We're in trouble
      continue
    }
    const name = SpecificId.getNameFromSpecificId(specificId)
    cachedInventory.inventory.items[i] = { ...item, ...specificId.getObject(), full_name: name }
  }
  await attachUrlsForSpecificIds(cachedInventory.inventory.items)
  log.info(`Returning cached inventory for ${steamid} - with ${cachedInventory.inventory?.items?.length} items (from ${new Date(cachedInventory.inventory?.date || 0)})`)
  return { status: { ...status, cacheAge: cachedInventory.cacheAge }, inventory: cachedInventory.inventory }
}

async function updateKnownSpecificIds(items) {
  const staged = []
  const added = []
  for (let i = 0; i < items.length; i++) {
    const item = items[i]
    if (added.indexOf(item.specific_id) > -1) continue
    added.push(item.specific_id)
    staged.push({ specific_id: item.specific_id, url: item.url.startsWith('http') ? item.url : `https://steamcommunity-a.akamaihd.net/economy/image/${item.url}` })
  }
  Mongo.saveBulk(staged, 'specific_id', mSpecificIdItem)
}

async function isInventoryPrivate(steamid) {
  const privates = ['The specified profile could not be found', 'inventory is currently private']
  const response = await makeSteamRequest(`https://steamcommunity.com/profiles/${steamid}/inventory`)
  if (response.statusCode === 200) {
    return privates.find(o => response.body.indexOf(o) > -1)
  }
  return null
}

/**
 * Creates a cloned inventory with only item ids and specific_ids and saves to db
 */
function saveInventoryToMongo(inventory) {
  const items = {}
  inventory.items.forEach(item => { items[item.id] = item.specific_id })
  const cloneInv = Object.assign({}, inventory)
  cloneInv.items = items
  Mongo.saveBulk([cloneInv], 'steamid64', MongoInventory)
}

/**
 * Saves cloned versions of items so that we don't have to wait for the save to modify the original items
 */
function saveInstanceItemsToMongo(steamid64, items) {
  const week = getDropWeek(Date.now())
  const instanceItems = items.map(item => { return { id: item.id, specific_id: item.specific_id, steamid64, lastWrite: Date.now(), $setOnInsert: { time: Date.now() } } })
  const creators = items.map(item => { return { id: item.id, $setOnInsert: { steamid64, time: Date.now(), week } } })
  if (instanceItems.length === 0) return
  Mongo.saveBulk(instanceItems, 'id', MongoInstanceItem)
  Mongo.saveBulk(creators, 'id', mInstanceItemCreator)
}

/**
 * Gets an inventory from Steam. Writes directly to passed in inventory object.
 * May return null if all attempts were blocked/ratelimited/service unavailable
 * @returns {{assets: [], descriptions: []} | null}
 */
async function getInventoryPart(steamid, part, inventory, timeout = 60000, count = 75) {
  const start = Date.now()
  let url = `https://steamcommunity.com/inventory/${steamid}/440/2?l=english&count=${count}`
  if (part) {
    url += `&start_assetid=${part}`
  }
  let body = null
  while (true) {
    const response = await SteamRequest.makeSteamRequest(url, `https://steamcommunity.com/profiles/${steamid}/inventory`)
    if (response === 'NoIP' || response.statusCode === 403) {
      return
    }
    body = response ? response.body : null
    if (!body) {
      await wait(1000)
      continue
    }
    break
  }
  inventory.parts++
  if (inventory.total_inventory_count) {
    inventory.assets.splice(inventory.assets.length, 0, body.assets)
    inventory.descriptions.splice(inventory.descriptions.length, 0, body.descriptions)
  } else {
    // inventory object is not populated yet - this might be the first call
    mapObjectOnto(body, inventory)
  }

  if (body.more_items === 1 && body.last_asset) {
    return await getInventoryPart(steamid, body.last_assetid, inventory, timeout - (Date.now() - start), 2000)
  }
  return inventory
}

function mapObjectOnto(src, dest) {
  const keys = Object.keys(src)
  for (let i = 0; i < keys.length; i++) {
    const key = keys[i]
    dest[key] = src[key]
  }
}

async function getItems(items) {
  if (items.length === 0) return []
  const mInstanceItems = await MongoInstanceItem.find({ $or: Object.keys(items).map(item => { return { id: item } }) }).select({ _id: 0 }).lean().exec()
  if (mInstanceItems.length === 0) return []
  const prices = await MongoPrice.find({ $or: mInstanceItems.map(item => { return { specific_id: item.specific_id } }) }).select({ _id: 0 }).lean().exec()
  const schemaItems = await MongoSchemaItem.find({ $or: mInstanceItems.map(item => { return { defindex: item.defindex } }) }).lean().exec()

  for (let i = 0; i < Object.keys(items).length; i++) {
    const itemId = Object.keys(items)[i]
    const instanceItem = mInstanceItems.find(item => item.id === itemId)
    if (!instanceItem) { continue }
    const schemaItem = schemaItems.find(item => item.defindex === instanceItem.defindex)
    const price = prices.find(item => item.specific_id === instanceItem.specific_id)
    if (!schemaItem) {
      continue
    }
    items[itemId] = { ...items[itemId], ...schemaItem, ...instanceItem }
    removeDebugPropertiesFromMongoInstanceItem(items[itemId])
    if (price) {
      items[itemId].price = price.avg_metal
    }
  }
  return items
}

async function getPricesFromEscrowData(items) {
  // {"ASSETID": {classid: "", instanceid: ""}}
  const urls = makeGetAssetClassInfoUrls(items)
  const descriptions = []
  for (let i = 0; i < urls.length; i++) {
    const url = urls[i]
    const response = await makeSteamRequest(url)
    if (!response) continue
    if (response.statusCode !== 200) {
      log.warn(`COULDN'T GETASSETCLASSINFO BECAUSE ${response.body} (status: ${response.statusCode})`)
      continue
    }
    delete response.body.result.success
    Object.keys(response.body.result).forEach(key => descriptions.push(response.body.result[key]))
  }
  // ... make request

  const assets = items
  const specificIds = resolveSpecificIds(assets, descriptions)
  await attachPricesToItems(specificIds)
  return specificIds.reduce((acc, obj) => { acc[obj.id] = obj.price; return acc }, {})
}

function makeGetAssetClassInfoUrls(items) {
  const urls = []
  const urlBase = `https://api.steampowered.com/ISteamEconomy/GetAssetClassInfo/v1?key=${steamApiKeys.getRandomKey()}&appid=440`
  items.sort((a, b) => Number(a.classid) > Number(b.classid) ? 1 : -1)
  const distinctItems = getUniqueClassInstances(items)

  let currentUrl = urlBase
  let added = 0
  for (let i = 0; i < distinctItems.length; i++) {
    const item = distinctItems[i]
    const { instanceid, classid } = item
    let part = `classidNX=${classid}`
    if (instanceid) {
      part += `&instanceidNX=${instanceid}`
    }
    if (currentUrl.length + part.length + 17 > 2000) {
      urls.push(`${currentUrl}&class_count=${added}`)
      currentUrl = urlBase
      added = 0
    }
    currentUrl = `${currentUrl}&${part.replace(/NX=/g, added + '=')}`
    added++
  }
  if (added > 0) {
    urls.push(`${currentUrl}&class_count=${added}`)
  }
  return urls
}

function getUniqueClassInstances(items) {
  const added = []
  for (let i = 0; i < items.length; i++) {
    const item = items[i]
    const { instanceid, classid } = item
    if (added.find(o => o.instanceid === instanceid && o.classid === classid)) continue
    added.push(item)
  }
  return added
}

function removeDebugPropertiesFromMongoInstanceItem(mInstanceItem) {
  delete mInstanceItem._id
  delete mInstanceItem['defindex_' + mInstanceItem.defindex]
  for (const property in mInstanceItem) {
    if (property.startsWith('assetid_')) {
      delete mInstanceItem[property]
    }
  }
}

/**
 * Looks up the most recent cache for an inventory.
 * Optionally applies projections to provide more recent data without needing to spam the Steam servers
 * @param {String} steamid64 steamid64 of the inventory to get
 * @param {Number} maxage The max age (in seconds) for this cache
 * @param {Boolean} allowProjections Whether we can apply projections to the cache or just return as-is
 * @returns {{inventory: {items: [], total_worth, total_price, metal_worth, date, steamid64}, stale, cacheAge} | null}
 */
async function getRecentInventoryCache(steamid64, maxage = 0, allowProjections = true) {
  try {
    let invManInventory = await MongoInventory.findOne({ steamid64: steamid64 }).select({ _id: 0, __v: 0 }).lean().exec()
    let gcInventory = await MongoGCInventory.findOne({ steamid64 }).select({ _id: 0, __v: 0 }).lean().exec()

    if (gcInventory && (!invManInventory || gcInventory.date > invManInventory.date)) {
      // Prefer to use the GC. Only need to hydrate - no apply projection
      const gcAge = Date.now() - gcInventory.date
      const gcStale = maxage > 0 && gcAge > (Math.ceil(maxage, MINIMUM_CACHE_FREQUENCY) * 1000)

      if (Object.keys(gcInventory.items).length > 0) {
        gcInventory.items = gcInventory.items ? Object.keys(gcInventory.items).map(key => {
          const specificId = gcInventory.items[key]
          return { id: key, specific_id: specificId, defindex: specificId.substring(0, specificId.indexOf(";")) }
        }) : []
        if (gcInventory.items.length === 0) return null
        const { totalWorth, metalWorth } = await attachPricesToItems(gcInventory.items)
        attachSchemaInfoToItems(gcInventory.items)

        gcInventory.total_worth = totalWorth
        gcInventory.total_price = totalWorth
        gcInventory.metal_worth = metalWorth
      } else {
        gcInventory.items = []
      }
      delete gcInventory._id
      log.info(`Returning GC Cache Inventory instead of InvMan own cache for ${steamid64}`)
      return { inventory: gcInventory, stale: gcStale, age: gcAge }
    }

    if (invManInventory) {
      const cacheAge = Date.now() - invManInventory.date
      const cacheStale = maxage > 0 && cacheAge > (Math.ceil(maxage, MINIMUM_CACHE_FREQUENCY) * 1000)
      if (allowProjections) {
        invManInventory = await applyProjectionsToMongoInventory(invManInventory)
      }

      if (Object.keys(invManInventory.items).length > 0) {
        invManInventory.items = invManInventory.items ? Object.keys(invManInventory.items).map(key => {
          const specificId = invManInventory.items[key]
          return { id: key, specific_id: specificId, defindex: specificId.substring(0, specificId.indexOf(";")) }
        }) : []
        if (invManInventory.items.length === 0) return null
        const { totalWorth, metalWorth } = await attachPricesToItems(invManInventory.items)
        attachSchemaInfoToItems(invManInventory.items)

        invManInventory.total_worth = totalWorth
        invManInventory.total_price = totalWorth
        invManInventory.metal_worth = metalWorth
      } else {
        invManInventory.items = []
      }
      delete invManInventory._id
      return { inventory: invManInventory, stale: cacheStale, age: cacheAge }
    }
  } catch (e) {
    log.error(e)
  }
}

/**
 * Adds/Removes items from the inventory based on self-reported data from node-toolbnox-wisp.
 * Allows for more up-to-date information without spamming steam servers
 * @param {{items, steamid64 }} inventory The cached inventory
 */
async function applyProjectionsToMongoInventory(inventory) {
  const changeSets = await MongoProjectionChangeSet.find({ steamid64: inventory.steamid64, time: { $gt: Math.max(inventory.date + 1, POST_FUCKERY_EPOCH) } }).lean().exec()
  // log.info(`There are ${changeSets.length} relevant changesets to apply`)
  if (changeSets.length === 0) { return inventory }
  const sortedChangeSets = changeSets.sort((a, b) => a.time < b.time ? -1 : a.time === b.time ? 0 : 1)
  let latestDateApplied = 0
  inventory.projected_changes = 0
  for (let i = 0; i < sortedChangeSets.length; i++) {
    const changeSet = sortedChangeSets[i]

    for (let a = 0; a < changeSet.removed_items.length; a++) {
      const removedItem = changeSet.removed_items[a]
      const item = inventory.items[removedItem]
      if (item) {
        delete inventory.items[removedItem]
        inventory.projected_changes++
        if (latestDateApplied < changeSet.time) {
          latestDateApplied = changeSet.time
        }
      } else {
        log.debug(`ChangeSet said that ${removedItem} was removed, but it already doesn't exist in inventory`)
      }
    }

    // for (let i = 0; i < changeSet.new_items.length; i++) {
    //   const newItem = changeSet.new_items[i]
    //   const index = inventory.items[newItem]
    //   if (!index) {
    //     inventory.items[newItem]
    //     inventory.projected_changes++
    //     if (latestDateApplied < changeSet.time) {
    //       latestDateApplied = changeSet.time
    //     }
    //   } else {
    //     // log.info('Projected changes wanted to add an item that already exists in inventory')
    //   }
    // }
  }
  if (latestDateApplied > 0) {
    inventory.projection_date = latestDateApplied
  }
  return inventory
}

/**
 * Takes a full sized inventory and groups the items array by specific_id
 * @param {{items: []}} inventory the Inventory object that has an items array
 */
function compressInventory(inventory) {
  const newItems = []
  for (let i = 0; i < inventory.items.length; i++) {
    const item = inventory.items[i]
    const existing = item.inputs || item.customized ? undefined : newItems.find(o => o.specific_id === item.specific_id)
    if (existing) {
      existing.instances.push({ original_id: item.original_id || 0, id: item.id })
    } else {
      item.instances = [{ original_id: item.original_id || 0, id: item.id }]
      newItems.push(item)
    }
    delete item.alt_name
    delete item.name
    delete item.original_id
    delete item.id
  }
  newItems.sort((a, b) => a.count < b.count ? 1 : -1)
  inventory.items = newItems
  return inventory
}

cacheItemSchema('4023CC71A20133A3FD64A74155416A1E')
setInterval(() => cacheItemSchema('4023CC71A20133A3FD64A74155416A1E'), 1000 * 60 * 60 * 24)
setInterval(cleanupOldJobs, 1000 * 60)
setInterval(logQueueWaiting, 60 * 1000)

let startMinute = getEpochMinute() - 1
module.exports = {
  events,
  SteamRequest,
  stats,
  getInventory,
  canWeHandleRequests,
  ItemSchema,
  getItems,
  attachPricesToItems,
  getPricesFromEscrowData,
  isBacklogged: () => /*stats.lastAvgWaitTime > 10000 ||*/ Object.keys(stats.activeJobs).length * 2 >= SteamRequest.avgUsableProxies(),
  getStats: () => {
    const api = {
      overview: `${SteamRequest.avgUsableProxies()} usable proxies (capacity: ${Math.floor(SteamRequest.maxRequestsPerMinute())} requests per minute)`,
      usable: SteamRequest.avgUsableProxies(),
      fluxManagerUrl: process.env.FLUX_PROXY_GATEWAY,
      avgWaitTimeMs: Math.round(stats.lastAvgWaitTime),
      avgWaitTimes: stats.avgWaitTimes,
      capacityPerMinute: SteamRequest.maxRequestsPerMinute(),
      waitingSids: Object.keys(stats.waitingSids),
      activeJobs: Object.values(stats.activeJobs).map(o => o.steamid)
    }
    const globalStats = {}
    const keys = Object.keys(SteamRequest.stats)
    for (let i = keys.length; i >= 0; i--) {
      const key = keys[i]
      globalStats[key] = SteamRequest.stats[key]
    }
    const average = arr => arr.reduce((p, c) => p + c, 0) / arr.length;
    api.globalStats = {
      ...globalStats,
      debugInventoryParts: Object.keys(stats.debugInventoryParts).reduce((acc, obj) => {
        acc[obj] = { count: stats.debugInventoryParts[obj].length, avgTime: average(stats.debugInventoryParts[obj]) }
        return acc
      }, {}),
      activeJobs: Object.keys(stats.activeJobs).length,
      waitingJobs: Object.keys(stats.waitingSids).length,
      requestsPerMinute: Object.keys(SteamRequest.stats.requestsPerMinute).reduce((acc, epochMinute) => {
        acc[epochMinute - startMinute] = `${SteamRequest.stats.requestsPerMinute[epochMinute]} (${SteamRequest.stats.successfulRequestsPerMinute[epochMinute]} successful)`
        return acc
      }, {}),
      knownIPs: undefined,
      perIP: SteamRequest.stats.knownIPs.map(o => {
        return {
          ...o,
          totalPerMinute: undefined,
          successesPerMinute: undefined,
          perMinute: Object.keys(o.totalPerMinute).reduce((acc, epochMinute) => {
            if (!o.totalPerMinute[epochMinute]) return acc
            acc[epochMinute - startMinute] = `${o.totalPerMinute[epochMinute]} (${o.successesPerMinute[epochMinute]} successful)`
            return acc
          }, {})
        }
      }).sort((a, b) => a.zombie ? 1 : -1)
    }
    return api
  }
}
