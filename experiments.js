const mInstanceItem = require('./dbApiTests/mInstanceItem')
const mInventoryCache = require('./dbApiTests/mInventoryCache')
const { estimateTimeLeft } = require('./helpers')
const { asyncCursor, saveBulk } = require('./mongo')
const { calculateDropsThisWeek } = require('./droplogger')
const mHistoricalChangeSet = require('./dbApiTests/mHistoricalChangeSet')
const xDeviceDrops = require('./dbApiTests/xDeviceDrops')
const xLeaderboard = require('./dbApiTests/xLeaderboard')
const xIdleSession = require('./dbApiTests/xIdleSession')
const mIdlerTimes = require('./dbApiTests/mIdlerTimes')
const log = require('pino')({ prettyPrint: { colorize: true, translateTime: true } }).child({ name: 'experiments' })

async function start () {
  // await generateIdledMinutesFromXIdleSessions()
}

async function generateIdledMinutesFromXIdleSessions () {
  const idlerTimesDocsByWeek = {}
  await asyncCursor('', xIdleSession.find({}, { week: 1, idlers: 1, duration: 1, launch: 1 }).lean(), async doc => {
    for (let i = 0; i < doc.idlers.length; i++) {
      const steamid64 = doc.idlers[i]
      let byWeek = idlerTimesDocsByWeek[doc.week]
      if (!byWeek) {
        byWeek = []
        idlerTimesDocsByWeek[doc.week] = byWeek
      }

      let idlerTimeDoc = byWeek.find(o => o.steamid64 === steamid64)
      if (!idlerTimeDoc) {
        idlerTimeDoc = { week: doc.week, steamid64, minutes: 0 }
        byWeek.push(idlerTimeDoc)
      }
      idlerTimeDoc.minutes += doc.duration - (doc.launch || 0)
    }
  }, { parallel: 100 })
  const arr = Object.values(idlerTimesDocsByWeek)
  for (let i = 0; i < arr.length; i++) {
    const idlerTimes = arr[i]
    await saveBulk(idlerTimes, ['week', 'steamid64'], mIdlerTimes)
  }
}

start()
