const Mongoose = require('mongoose')
const dbApiTests = require('./dbConnection')

const schema = new Mongoose.Schema({
  specific_id: { type: String, index: true, unique: true },
  url: { type: String }
}, { strict: false })

module.exports = dbApiTests.model('mSpecificIdItem', schema)
