const Mongoose = require('mongoose')
const dbApiTests = require('./dbConnection')

const schema = new Mongoose.Schema({
  steamid64: { type: String, index: true },
  time: { type: Number, index: true }
}, { strict: false })

module.exports = dbApiTests.model('mIdlerDrops2', schema)
