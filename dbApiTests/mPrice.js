const Mongoose = require('mongoose')
const dbApiTests = require('./dbConnection')

module.exports = dbApiTests.model('mPrice', new Mongoose.Schema({}, { collection: 'mpricesTS' }))
