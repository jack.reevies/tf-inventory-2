const Mongoose = require('mongoose')
const dbApiTests = require('./dbConnection')

const schema = new Mongoose.Schema({
  defindex: { type: String, index: true }
})

module.exports = dbApiTests.model('mSchemaItem', schema)
