const Mongoose = require('mongoose')
const dbApiTests = require('./dbConnection')

const xLeaderboard = dbApiTests.model('xLeaderboard', new Mongoose.Schema({
  week: { type: Number, index: true },
  leaderboard: { type: Array },
  calculatedAt: { type: Number },
  source: { type: String, index: true }
}, { strict: false }))

module.exports = xLeaderboard
