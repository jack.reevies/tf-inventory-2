const Mongoose = require('mongoose')
const dbApiTests = require('./dbConnection')

module.exports = dbApiTests.model('mInventoryWatchTimes', new Mongoose.Schema({
  steamid64: { type: String, index: true, unique: true },
  last_checked: Number
}, { strict: false }))
