const Mongoose = require('mongoose')
const db = require('./dbConnection')

const schema = new Mongoose.Schema({
  alias: { type: String, unique: true, index: true },
  device: { type: String, index: true },
  week: { type: Number, index: true },
  consumed: { type: Boolean, index: true }
}, { strict: false })

module.exports = db.model('xIdleSession', schema)
