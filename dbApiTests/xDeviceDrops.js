const Mongoose = require('mongoose')
const dbApiTests = require('./dbConnection')

const xDeviceDrops = dbApiTests.model('xDeviceDrop', new Mongoose.Schema({
  alias: { type: String, index: true },
  week: { type: Number, index: true },
  calculatedAt: { type: Number },
  end: { type: Number },
  start: { type: Number },
  f2pWorth: { type: Number },
  metalWorth: { type: Number },
  totalWorth: { type: Number },
  items: { type: Object }
}, { strict: false }))

module.exports = xDeviceDrops
