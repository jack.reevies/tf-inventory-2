const Mongoose = require('mongoose')
const dbApiTests = require('./dbConnection')

const schema = new Mongoose.Schema({
  steamid64: { type: String, index: true },
  week: { type: Number, index: true },
  minutes: { type: Number }
}, { strict: false })

module.exports = dbApiTests.model('mIdlerTimes', schema)
