const Mongoose = require('mongoose')
const dbApiTests = Mongoose.createConnection('mongodb://te:trading3xpress@178.128.37.239:27017', {
  // sets how many times to try reconnecting
  reconnectTries: Number.MAX_VALUE,
  // sets the delay between every retry (milliseconds)
  reconnectInterval: 1000
})

module.exports = dbApiTests
