const Mongoose = require('mongoose')
const dbApiTests = require('./dbConnection')

const schema = new Mongoose.Schema({
  alias: { type: String, unique: true }
}, { strict: false })

module.exports = dbApiTests.model('mDeviceIdlers', schema)
