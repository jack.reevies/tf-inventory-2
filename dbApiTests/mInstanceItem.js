const Mongoose = require('mongoose')
const dbApiTests = require('./dbConnection')
// const Explain = require('mongoose-explain')

const schema = new Mongoose.Schema({
  original_id: { type: String, index: true },
  id: { type: String, index: true },
  defindex: String,
  time: { type: Number, index: true },
  tbxTime: { type: Number, index: true },
  steamid64: { type: String, index: true },
  lastWrite: { type: Number, index: true },
  specific_id: { type: String, index: true },
  backupTime: { type: Number, index: true },
  backupTime2: { type: Number, index: true },
  applied: { type: Boolean, index: true },
  applied2: { type: Boolean, index: true }
}, { strict: false })

// schema.plugin(Explain)
module.exports = dbApiTests.model('mInstanceItem', schema)
