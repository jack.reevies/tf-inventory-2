const Mongoose = require('mongoose')
const dbApiTests = require('./dbConnection')

module.exports = dbApiTests.model('mInventoryCache', new Mongoose.Schema({
  steamid64: { type: String, index: true },
  dropweek: { type: Number, index: true },
  addedTimes: { type: Number, index: true },
  date: { type: Number, index: true },
  minTimesAdded: { type: Boolean, index: true }
}, { strict: false }))
