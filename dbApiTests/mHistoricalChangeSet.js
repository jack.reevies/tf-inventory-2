const Mongoose = require('mongoose')
const dbApiTests = require('./dbConnection')
// const Explain = require('mongoose-explain')

const schema = new Mongoose.Schema({
  steamid64: { type: String, index: true },
  week: { type: Number, index: true }
}, { strict: false })

// schema.plugin(Explain)
module.exports = dbApiTests.model('mChangeSet', schema)
