const Mongoose = require('mongoose')
const dbApiTests = require('./dbConnection')
// const Explain = require('mongoose-explain')

const schema = new Mongoose.Schema({
  week: { type: Number, index: true, unique: true }
}, { strict: false })

// schema.plugin(Explain)
module.exports = dbApiTests.model('mLeaderboard', schema)
