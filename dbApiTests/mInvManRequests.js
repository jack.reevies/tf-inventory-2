const Mongoose = require('mongoose')
const dbApiTests = require('./dbConnection')

const schema = new Mongoose.Schema({
  minute: { type: Number, unique: true }
}, { strict: false })

module.exports = dbApiTests.model('mInvManRequests', schema)
