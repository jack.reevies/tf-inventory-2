const Mongoose = require('mongoose')
const dbApiTests = require('./dbConnection')

module.exports = dbApiTests.model('mInventory', new Mongoose.Schema({
  steamid64: { type: String, index: true, unique: true }
}, { strict: false }))
