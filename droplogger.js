const Inventory = require('./inventory')
const mWatchTime = require('./dbApiTests/mInventoryWatchTimes')
const mDeviceIdlers = require('./dbApiTests/mDeviceIdlers')
const { getDropWeek, getTimeFromDropWeekNumber, wait, waitForSchema, getYearlyWeekNumber, estimateTimeLeft, SPECIFIC_IDS_METALS, timeFunction } = require('./helpers')
const { sayDroploggerDead } = require('./discord')
const SpecificId = require('@spacepumpkin/tf-specific-id')
const mInventoryWatchTimes = require('./dbApiTests/mInventoryWatchTimes')
const { attachPricesToItems, attachUrlsForSpecificIds } = require('./items')
const xLeaderboard = require('./dbApiTests/xLeaderboard')
const xDeviceDrops = require('./dbApiTests/xDeviceDrops')
const xIdleSession = require('./dbApiTests/xIdleSession')
const mInstanceItem = require('./dbApiTests/mInstanceItem')
const INTERVAL_DEFAULT = 1000 * 60 * 60 * 24
const INTERVAL_NO_DROP = 1000 * 60 * 60 * 24
const INTERVAL_DROP = 1000 * 60 * 60 * 1
const log = require('pino')({ prettyPrint: { colorize: true, translateTime: true } }).child({ name: 'droplogger' })
const WAIT_TIMEOUT = 1000 * 60 * 5
const QUEUE_REFRESH_INTERVAL = 1000 * 60 * 10
const { asyncCursor, saveBulk } = require('./mongo')
const { mongo } = require('mongoose')
const SteamRequest = require('./steamRequest')

const stats = {
  perRefresh: 0,
  refreshInterval: timeAgo(QUEUE_REFRESH_INTERVAL),
  queueSize: 0,
  lastRefreshCount: 0,
  lastDuration: 0,
  estimatedQueueTime: 0
}

let queue = []
let processed = 0

Inventory.events.on('inventory', async obj => {
  /** @type {{items, steamid64, date}} */
  const inventory = obj.inventory
  // We got a new inventory from steam
  const steamid64Query = { steamid64: inventory.steamid64 }
  const watchDoc = await mWatchTime.findOne(steamid64Query).lean().exec()

  if (!watchDoc) return

  // We want to watch this inventory

  let interval = INTERVAL_NO_DROP
  let validOverride = false

  if (watchDoc.interval_expires && watchDoc.interval_expires > Date.now()) {
    // We have a valid override interval (maybe we expect there to be drops soon)
    interval = watchDoc.interval
    validOverride = true
  }

  const update = { interval, next_check: Date.now() + interval, last_checked: Date.now() }
  if (!validOverride) {
    update.$unset = { interval_expires: 1 }
  }

  mWatchTime.updateOne(steamid64Query, update, { upsert: true }).exec()
})

let droploggerDeadWarn = 0
function droploggerWatcher() {
  if (stats.lastUpdated === 0 || stats.lastUpdated > Date.now() - (1000 * 60 * 5) || queue.length < 5) { return }
  const minutesAgo = `${Math.floor((Date.now() - stats.lastUpdated) / 60000)} minutes ago`
  log.warn(`Droplogger has been quiet for over 5 minutes (last action was: ${minutesAgo})`)
  if (droploggerDeadWarn > Date.now() - (1000 * 60 * 60)) { return }
  sayDroploggerDead(new Date(stats.lastUpdated).toString(), queue.length)
  droploggerDeadWarn = Date.now()
}

setInterval(droploggerWatcher, 1000 * 60 * 10)

function refreshQueue() {
  mWatchTime.find({}).countDocuments().exec().then(size => {
    stats.queueSize = size
  })

  mWatchTime.find({}).sort({ next_check: 1 }).select({ steamid64: 1, interval: 1, last_checked: 1 }).exec().then(docs => {
    queue = docs.map(o => o._doc)
  })
  if (processed) {
    stats.perRefresh = processed
    processed = 0
    const milliPerSteam = QUEUE_REFRESH_INTERVAL / stats.perRefresh
    const estimatedMillisForQueue = milliPerSteam * stats.queueSize
    stats.estimatedQueueTime = timeAgo(estimatedMillisForQueue)
  }
}

async function processQueue() {
  await waitForSchema(Inventory.ItemSchema)
  refreshQueue()
  setInterval(refreshQueue, QUEUE_REFRESH_INTERVAL)

  while (true) {
    await waitForLowQueue()
    if (queue.length === 0) { continue }
    const doc = queue.splice(0, 1)[0]

    if (!doc.steamid64) continue

    Inventory.getInventory(doc.steamid64, 120, 12000, false, false).then(res => {
      if (res && res.inventory && res.inventory.isPrivate) {
        mWatchTime.deleteOne({ steamid64: doc.steamid64 }).exec()
      }
    })
    log.info(`Droplogger: Sent in ${doc.steamid64} for refresh (last refresh was ${timeAgo(Date.now() - doc.last_checked)} ago) (update interval of ${timeAgo(doc.interval)}) (queue time: ${stats.estimatedQueueTime}) (this refresh: ${processed} / last refresh: ${stats.perRefresh}) (${Object.keys(Inventory.stats.activeJobs).length} in queue)`)
    processed++

    stats.lastUpdated = Date.now()
  }
}

function timeAgo(ms) {
  const HOUR = 1000 * 60 * 60
  const MINUTE = 1000 * 60
  const SECOND = 1000

  if (ms > HOUR) {
    return `${Math.floor((ms / HOUR) * 100) / 100} hours`
  } else if (ms > MINUTE) {
    return `${Math.floor((ms / MINUTE) * 100) / 100} minutes`
  } else if (ms > SECOND) {
    return `${Math.floor((ms / SECOND) * 100) / 100} seconds`
  } else {
    return `${ms}ms`
  }
}

async function waitForLowQueue(size = 2, forMs = 1000, timeout = WAIT_TIMEOUT) {
  let toTimeout = timeout
  await wait(10)
  while (Inventory.isBacklogged() && toTimeout > 0) {
    toTimeout--
    await wait(1000)
  }
}

async function watchInventoriesFor(steamids, hours) {
  const expiry = 1000 * 60 * 60 * (hours || 12) + Date.now()
  for (let i = 0; i < steamids.length; i++) {
    const steamid = steamids[i]
    mInventoryWatchTimes.updateOne({ steamid64: steamid }, {
      steamid64: steamid,
      interval: INTERVAL_DROP,
      interval_expires: expiry,
      next_check: Date.now(),
      last_checked: Date.now() - INTERVAL_DROP
    }, { upsert: true }).exec()
  }
  log.info(`Set watch times for ${steamids.length} steamids to ${INTERVAL_DROP}`)
}

async function associateIdlersWithDevice(alias, steamid64s) {
  log.info(`Updating ${alias} with ${steamid64s.length} known idlers`)
  const deviceIdlers = await mDeviceIdlers.find({}).lean().exec()

  for (const steamid64 in steamid64s) {
    for (const device of deviceIdlers) {
      if (device.alias === alias) continue
      delete device.steamid64s[steamid64]
    }
  }

  for (const device of deviceIdlers) {
    if (device.alias === alias) continue
    await mDeviceIdlers.deleteOne({ alias: device.alias }).exec()
    log.info(`${device.alias} now has ${Object.keys(device.steamid64s).length} idlers`)
    await mDeviceIdlers.updateOne({ alias: device.alias }, { steamid64s: device.steamid64s }, { upsert: true }).exec()
  }

  await mDeviceIdlers.updateOne({ alias }, { steamid64s }, { upsert: true }).exec()

  createWatchTimesForMissingSteamid64s(steamid64s)
}

async function createWatchTimesForMissingSteamid64s(steamid64s) {
  const watchTimes = {}
  const keys = Object.keys(steamid64s)
  for (let i = 0; i < keys.length; i++) {
    const steamid64 = keys[i];
    watchTimes[steamid64] = { steamid64, last_checked: 0, next_check: Date.now(), interval: INTERVAL_DEFAULT }
  }
  await asyncCursor('', mWatchTime.find({ $or: keys.map(o => { return { steamid64: o } }) }).lean(), async doc => {
    if (watchTimes[doc.steamid64] && watchTimes[doc.steamid64].interval && watchTimes[doc.steamid64].interval <= INTERVAL_DEFAULT) {
      watchTimes[doc.steamid64] = undefined
      delete watchTimes[doc.steamid64]
    }
  })
  if (!Object.values(watchTimes).length) return
  await saveBulk(Object.values(watchTimes).filter(o => o), "steamid64", mWatchTime)
}

async function getDropsForSession(sessionAlias) {
  const session = await xIdleSession.findOne({ alias: sessionAlias }, { idlers: 1, start: 1, end: 1 }).lean().exec()
  const items = await mInstanceItem.find({ tbxTime: { $gt: session.start, $lte: session.end }, $or: session.idlers.map(o => { return { steamid64: o } }) }, { specific_id: 1, id: 1 }).lean().exec()

  const { totalWorth, metalWorth, f2pWorth, totalItems, metalItems, f2pItems } = await attachPricesToItems(items)
  await attachUrlsForSpecificIds(items)

  const itemsObj = items.reduce((acc, obj) => {
    if (!acc[obj.specific_id]) {
      const spid = SpecificId.fromOwnId(obj.specific_id)
      acc[obj.specific_id] = { ...obj, ...spid.getObject(), name: SpecificId.getNameFromSpecificId(spid), instances: {} }
      delete acc[obj.specific_id].id
    }
    acc[obj.specific_id].instances[obj.id] = session.end
    return acc
  }, {})

  return { items: itemsObj, totalWorth, metalWorth, f2pWorth, totalItems, metalItems, f2pItems }
}

async function getDeviceDropsForWeek(deviceName, week, source) {
  return xDeviceDrops.findOne({ alias: deviceName, week, source }).lean().exec()
}

async function getLeaderboardOverview() {
  const leaderboard = await xLeaderboard.find({}, { _id: 0, __v: 0 }).lean().exec()
  leaderboard.sort((a, b) => a.week > b.week ? 1 : -1)

  const deviceDrops = await xDeviceDrops.find({}, { _id: 0, __v: 0, items: 0, srcSessions: 0 }).lean().exec()

  const deviceDropsByWeek = deviceDrops.reduce((acc, obj) => {
    if (!acc[obj.week]) { acc[obj.week] = [] }
    acc[obj.week].push(obj)
    return acc
  }, {})

  for (let i = 0; i < leaderboard.length; i++) {
    const week = leaderboard[i]
    week.weeksAgo = getDropWeek(Date.now()) - week.week
    week.start = getTimeFromDropWeekNumber(week.week)
    week.end = getTimeFromDropWeekNumber(week.week + 1)
    const { weekNumber, year } = getYearlyWeekNumber(week.start + 1)
    week.weekOfYear = weekNumber
    week.year = year

    for (let x = 0; x < week.leaderboard.length; x++) {
      const device = week.leaderboard[x]
      week.leaderboard[x] = deviceDropsByWeek[week.week].find(o => o.alias === device && o.source === week.source)
    }
    week.leaderboard = week.leaderboard.filter(o => o && o.alias)
  }
  return leaderboard
}

function compressLeaderboardDrops(items) {
  const newItems = {}
  for (let i = 0; i < items.length; i++) {
    const item = items[i]
    if (!newItems[item.specific_id]) {
      newItems[item.specific_id] = { ...item, instances: {} }
      delete newItems[item.specific_id].specific_id
      delete newItems[item.specific_id].id
      delete newItems[item.specific_id].time
    }
    newItems[item.specific_id].instances[item.id] = item.time
  }
  return newItems
}

async function findEarliestUnprocessedIdleSession() {
  const idleSessions = await xIdleSession.findOne({ processed: { $exists: false } }).sort({ week: 1 }).lean().exec()
  return idleSessions ? idleSessions.week : null
}

async function createLeaderboardFromTBXSessions() {
  const start = Date.now()
  const startWeek = await findEarliestUnprocessedIdleSession()
  if (!startWeek) {
    log.info('No unprocessed xIdleSessions')
    return
  }
  const endWeek = getDropWeek(Date.now())
  let progress = 0
  const interval = setInterval(() => {
    const estimate = estimateTimeLeft(start, (endWeek - progress) / (endWeek - startWeek))
    log.info(`Processing week ${progress} out of ${endWeek} - ETA ${new Date(Date.now() + estimate * 1000).toString()}`)
  }, 10000)

  const devices = await mDeviceIdlers.find({}).lean().exec()

  for (let i = endWeek; i >= startWeek; i--) {
    progress = i
    const dropsByDevice = devices.reduce((acc, obj) => {
      acc[obj.alias.toUpperCase()] = { alias: obj.alias.toUpperCase(), week: i, items: {}, source: 'tbx2', f2pItems: 0, f2pWorth: 0, metalItems: 0, metalWorth: 0, totalDrops: 0, totalItems: 0, totalWorth: 0, worth: 0 }
      return acc
    }, {})

    const leaderboard = { week: i, leaderboard: [] }
    await asyncCursor('', xIdleSession.find({ week: i }, { device: 1, drops: 1, ref: 1, alias: 1 }).lean(), async doc => {
      let ownerDevice = dropsByDevice[doc.device]
      if (!ownerDevice) {
        ownerDevice = { alias: doc.device.toUpperCase(), week: i, items: {}, source: 'tbx2', f2pItems: 0, f2pWorth: 0, metalItems: 0, metalWorth: 0, totalDrops: 0, totalItems: 0, totalWorth: 0, worth: 0 }
        dropsByDevice[ownerDevice.alias] = ownerDevice
      }

      ownerDevice.totalItems += Number(doc.drops) || 0
      ownerDevice.totalWorth += (Number(doc.ref) || 0) * 18
      xIdleSession.updateOne({ alias: doc.alias }, { processed: Date.now() }).exec()
    })

    const values = Object.values(dropsByDevice)
    values.forEach(o => { o.worth = o.totalWorth })
    values.sort((a, b) => a.totalWorth > b.totalWorth ? -1 : 1)
    leaderboard.leaderboard = values.map(o => o.alias)
    await Promise.all(values.map(o => xDeviceDrops.updateOne({ alias: o.alias, week: o.week, source: 'tbx2' }, o, { upsert: true }).exec()))
    await xLeaderboard.updateOne({ week: i, source: 'tbx2' }, leaderboard, { upsert: true }).exec()
  }
  clearInterval(interval)
}

async function fillInDeviceDropGapsForTBX() {
  while (true) {
    const idleSession = await xIdleSession.findOne({ consumed: { $exists: false } }, { alias: 1, device: 1, week: 1 }).lean().exec()
    if (!idleSession) break
    // We have an idleSession that needs to be processed. We can pull down a specific xDeviceDrop for this
    // Then we need to find ALL unprocessed xIdleSessions for the same device and week
    const idleSessions = await xIdleSession.find({ consumed: { $exists: false }, device: idleSession.device, week: idleSession.week }, { items: 1, end: 1, alias: 1 }).lean().exec()

    // Now we have all unprocessed xIdleSessions that we go into creating/updating a single xDeviceDrop

    let deviceDrop = await xDeviceDrops.findOne({ source: 'tbx2', week: idleSession.week, alias: idleSession.device }, { items: 1, alias: 1, week: 1 }).lean().exec()

    if (!deviceDrop) {
      deviceDrop = {
        alias: idleSession.device,
        week: idleSession.week,
        source: 'tbx2',
        items: {},
        createdAt: Date.now(),
        history: {},
        updatedAt: Date.now(),
        metalWorth: 0,
        metalItems: 0,
        totalWorth: 0,
        totalItems: 0,
        f2pItems: 0,
        f2pWorth: 0,
        worth: 0
      }
    }

    Object.keys(deviceDrop.items).forEach(specificId => {
      deviceDrop.items[specificId].specific_id = specificId
    })

    let changesMade = 0
    const initialItemCount = Object.values(deviceDrop.items).reduce((acc, obj) => acc + (Object.keys(obj.instances).length || 1), 0)
    for (let i = 0; i < idleSessions.length; i++) {
      const idleSession = idleSessions[i]
      Object.values(idleSession.items).forEach(dropsByAcc => {
        Object.keys(dropsByAcc).forEach(itemid => {
          const specificId = dropsByAcc[itemid]
          if (!deviceDrop.items[specificId]) {
            const spid = SpecificId.fromOwnId(specificId)
            deviceDrop.items[specificId] = { ...spid.getObject(), name: SpecificId.getNameFromSpecificId(spid), specific_id: specificId, price: 0, url: '', instances: {} }
          }
          if (!deviceDrop.items[specificId].instances[itemid]) {
            deviceDrop.items[specificId].instances[itemid] = idleSession.end
            changesMade++
          }
        })
      })
    }
    if (changesMade) {
      // Attach price and url to items
      const itemsArr = Object.values(deviceDrop.items)
      await attachPricesToItems(itemsArr)
      await attachUrlsForSpecificIds(itemsArr)
      deviceDrop.items = itemsArr.reduce((acc, obj) => {
        acc[obj.specific_id] = obj
        delete obj.specific_id
        return acc
      }, {})
      log.info(`Got device drops for ${deviceDrop.alias} for week ${deviceDrop.week}. Added ${changesMade} items to ${initialItemCount} already logged items`)
      await xDeviceDrops.updateOne({ alias: deviceDrop.alias, week: deviceDrop.week, source: 'tbx2' }, { items: deviceDrop.items, updatedAt: Date.now() }, { upsert: true }).exec()
    }

    await Promise.all(idleSessions.map(o => xIdleSession.updateOne({ alias: o.alias }, { consumed: true }).exec()))

    await wait(1000)
  }
}

async function calculateDropsThisWeek(week, source = 'invman') {
  if (!week) {
    week = getDropWeek(Date.now())
  }
  let leaderboard = await xLeaderboard.findOne({ week, source }, { _id: 0, __v: 0 }).lean().exec()
  let startTime = getTimeFromDropWeekNumber(week)
  const endTime = getTimeFromDropWeekNumber(week + 1)

  if (leaderboard) {
    startTime = leaderboard.calculatedAt
  } else {
    leaderboard = { week, leaderboard: [] }
  }
  const devices = await mDeviceIdlers.find({}).lean().exec()

  const query = { time: { $gte: startTime, $lt: endTime } }
  if (source === 'tbx') {
    query.tbxTime = query.time
    delete query.time
  }

  const count = await mInstanceItem.countDocuments(query).exec()
  log.info(`${count} new instance items logged (${source}) for week ${week} since last check (${leaderboard.calculatedAt ? new Date(leaderboard.calculatedAt).toString() : 'never'})`)

  const interval = setInterval(() => {
    const secondsToFinish = estimateTimeLeft(start, progress / count)
    log.info(`Processed ${progress} of ${count} items for week ${week} (ETA: ${new Date(Date.now() + (secondsToFinish * 1000)).toString()}) (${(progress / count) * 100} %)`)
  }, 10000)
  leaderboard.calculatedAt = Date.now()

  let progress = 0
  let changesMade = false
  const start = Date.now()
  let debugNoSpIds = 0
  await asyncCursor('', mInstanceItem.find(query, { steamid64: 1, specific_id: 1, id: 1, time: 1, tbxTime: 1 }).lean(), async (doc, i) => {
    progress = i
    if (!doc.steamid64) return
    if (!doc.specific_id) {
      debugNoSpIds++
      return
    }
    const specificId = SpecificId.fromOwnId(doc.specific_id, false)
    if (!specificId) {
      log.warn(`Could not interpret specific_id for ${doc.id} (${doc.specific_id})`)
      return
    }
    const ownerDevice = devices.find(o => Object.keys(o.steamid64s).indexOf(doc.steamid64) > -1)
    if (!ownerDevice) {
      // log.warn(`Couldn't find the device that owns the idler that got ${doc.id} (steamid: ${doc.steamid64})`)
      return
    }
    if (!ownerDevice.items) {
      ownerDevice.items = []
    }
    const name = SpecificId.getNameFromSpecificId(specificId)
    ownerDevice.items.push({ id: doc.id, specific_id: doc.specific_id, time: doc.time || doc.tbxTime, ...specificId.getObject(), name })
    changesMade = true
  })

  if (debugNoSpIds > 0) {
    log.warn(`There are ${debugNoSpIds} instance items with no specific ids in db`)
  }

  clearInterval(interval)

  if (!changesMade) return

  const deviceDrops = await xDeviceDrops.find({ week, source }).lean().exec()

  const tmpLeaderboard = []

  for (let i = 0; i < devices.length; i++) {
    const device = devices[i]
    const { totalWorth, metalWorth, f2pWorth, totalItems, metalItems, f2pItems } = await attachPricesToItems(device.items || [])
    await attachUrlsForSpecificIds(device.items || [])
    const compressedDrops = device.items ? compressLeaderboardDrops(device.items) : {}
    const existingXDeviceDrop = deviceDrops.find(o => o.alias === device.alias)

    if (!existingXDeviceDrop) {
      const history = {}
      history[Date.now()] = totalItems
      await xDeviceDrops.updateOne({ alias: device.alias, week, source }, {
        totalDrops: totalItems,
        totalItems: totalItems,
        metalItems: metalItems,
        totalWorth: totalWorth,
        metalWorth: metalWorth,
        f2pWorth: f2pWorth,
        f2pItems: f2pItems,
        items: compressedDrops,
        worth: totalWorth - metalWorth,
        source,
        createdAt: Date.now(),
        updatedAt: Date.now(),
        history
      }, { upsert: true }).exec()
      tmpLeaderboard.push({ alias: device.alias, worth: totalWorth - metalWorth })
      continue
    }

    const specificIds = Object.keys(compressedDrops)
    for (let i = 0; i < specificIds.length; i++) {
      const specificId = specificIds[i]
      const itemGroup = compressedDrops[specificId]
      if (existingXDeviceDrop.items[specificId]) {
        existingXDeviceDrop.items[specificId].instances = { ...existingXDeviceDrop.items[specificId].instances, ...itemGroup.instances }
      } else {
        existingXDeviceDrop.items[specificId] = compressedDrops[specificId]
      }
    }

    let newTotalItems = 0
    let newTotalWorth = 0
    let newMetalItems = 0
    let newMetalWorth = 0
    let newF2PWorth = 0
    let newF2PItems = 0

    const keys = Object.keys(existingXDeviceDrop.items)
    for (let i = 0; i < keys.length; i++) {
      const specificId = keys[i]
      const itemGroup = existingXDeviceDrop.items[specificId]

      const itemCount = Object.keys(itemGroup.instances).length
      newTotalItems += itemCount
      newTotalWorth += (itemGroup.price * itemCount)
      if (SPECIFIC_IDS_METALS.indexOf(specificId) > -1) {
        newMetalItems += itemCount
        newMetalWorth += (itemGroup.price * itemCount)
      }
      if (specificId.indexOf(';untradable') > -1) {
        newF2PWorth += ((itemGroup.f2pPrice || 0) * itemCount)
        newF2PItems += itemCount
      }
    }
    const addedTotalItems = newTotalItems - existingXDeviceDrop.totalDrops
    existingXDeviceDrop.totalDrops = existingXDeviceDrop.totalItems = newTotalItems
    existingXDeviceDrop.metalItems = newMetalItems
    existingXDeviceDrop.totalWorth = newTotalWorth
    existingXDeviceDrop.metalWorth = newMetalWorth
    existingXDeviceDrop.f2pWorth = newF2PWorth
    existingXDeviceDrop.f2pItems = newF2PItems
    existingXDeviceDrop.worth = newTotalWorth - newMetalWorth
    existingXDeviceDrop.updatedAt = Date.now()
    if (!existingXDeviceDrop.history) {
      existingXDeviceDrop.history = {}
    }
    existingXDeviceDrop.history[Date.now()] = addedTotalItems
    tmpLeaderboard.push({ alias: device.alias, worth: existingXDeviceDrop.worth })

    if (!Object.values(compressedDrops).length) continue

    await xDeviceDrops.updateOne({ alias: device.alias, week, source }, existingXDeviceDrop).exec()
  }

  tmpLeaderboard.sort((a, b) => a.worth > b.worth ? -1 : 1)
  leaderboard.leaderboard = tmpLeaderboard.map(o => o.alias)
  leaderboard.calculatedAt = Date.now()
  leaderboard.source = source

  await xLeaderboard.updateOne({ week, source }, leaderboard, { upsert: true }).exec()
  log.info(`Updated leaderboard for week ${week} with ${count} items`)
}

async function start() {
  processQueue()
  while (true) {
    log.info(`Took ${(await timeFunction(createLeaderboardFromTBXSessions)).ms}ms to createLeaderboardFromTBXSessions`)
    log.info(`Took ${(await timeFunction(fillInDeviceDropGapsForTBX)).ms}ms to createLeaderboardFromTBXSessions`)
    log.info(`Took ${(await timeFunction(calculateDropsThisWeek)).ms}ms to calculateDropsThisWeek`)
    await calculateDropsThisWeek(getDropWeek(Date.now() - 1)) // Get drops for last week too (to fill in that 30 mins gap)
    log.info('Waiting for 30 mins before next check')
    await wait(30 * 60 * 1000)
  }
}

module.exports = {
  stats,
  createWatchTimesForMissingSteamid64s,
  watchInventoriesFor,
  associateIdlersWithDevice,
  getDropsForSession,
  start,
  getLeaderboardOverview,
  getDeviceDropsForWeek,
  calculateDropsThisWeek
}
