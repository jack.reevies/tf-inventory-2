const { get } = require('@spacepumpkin/minimal-request')
const FIRST_DROP_WEEK = 1242864000000
const WEEK_EPOCH = 1000 * 60 * 60 * 24 * 7
module.exports.SPECIFIC_IDS_METALS = ['5000;6', '5001;6', '5002;6']
module.exports.NO_DROP_SPECIFIC_IDS = [...module.exports.SPECIFIC_IDS_METALS, '5021;6']

// Format the up time in a human readable way
module.exports.convertToTimestamp = function convertToTimestamp (time) {
  let upTime = time
  if (time < 0) return `${time} ms`
  const days = Math.floor(upTime / (1000 * 60 * 60 * 24))
  upTime -= days * (1000 * 60 * 60 * 24)

  const hours = Math.floor(upTime / (1000 * 60 * 60))
  upTime -= hours * (1000 * 60 * 60)

  const mins = Math.floor(upTime / (1000 * 60))
  upTime -= mins * (1000 * 60)

  if (days === 0) {
    const seconds = Math.floor(upTime / (1000))
    upTime -= seconds * (1000)
    return `${hours} hours, ${mins} mins, ${seconds} seconds`
  } else {
    return `${days} days, ${hours} hours, ${mins} mins`
  }
}

module.exports.timedPromise = function timedPromise (ms, promise) {
  // Create a promise that rejects in <ms> milliseconds
  const timeout = new Promise((resolve, reject) => {
    const id = setTimeout(() => {
      clearTimeout(id)
      reject(new Error(`Promise took longer than ${ms}ms to complete`))
    }, ms)
  })

  // Returns a race between our timeout and the passed in promise
  return Promise.race([
    promise,
    timeout
  ])
}

/**
 * Calculates how many seconds left given when the task started and the progress thats already been made
 * @param {Number} startTime Time in epoch (ms)
 * @param {Number} percent between 0 - 1
 * @returns {Number} How many seconds left
 */
module.exports.estimateTimeLeft = function estimateTimeLeft (startTime, percent) {
  const now = Date.now()
  const duration = now - startTime
  const estimatedLength = duration / percent
  const estimatedTimeLeft = estimatedLength - duration
  return Math.round(estimatedTimeLeft / 1000)
}

module.exports.getPublicIp = async function getPublicIp () {
  const response = await get('https://api.ipify.org')
  return response.body
}

module.exports.getDropWeek = function getDropWeek (epoch) {
  let week = 0
  let tmpEpoch = FIRST_DROP_WEEK
  while (tmpEpoch < epoch) {
    tmpEpoch += WEEK_EPOCH
    week++
  }
  return week + 1
}

module.exports.getTimeFromDropWeekNumber = function getTimeFromDropWeekNumber (dropWeekNumber) {
  return FIRST_DROP_WEEK + (WEEK_EPOCH * (dropWeekNumber - 2))
}

module.exports.getTimeFromWeekNumber = function getTimeFromWeekNumber (weekNumber, year) {
  const thursday = firstThursdayOfYear(year)
  return thursday.getTime() + (1000 * 60 * 60 * 24 * 7 * (weekNumber - 1))
}

function firstThursdayOfYear (year) {
  let day = 1
  while (true) {
    const date = new Date(Date.UTC(year, 0, day, 0, 0, 0, 0))
    if (date.getUTCDay() === 4) { // Thursday
      return date
    }
    day++
  }
}

module.exports.getYearlyWeekNumber = function getYearlyWeekNumber (epoch) {
  const date = new Date(epoch)
  const year = date.getUTCFullYear()
  const firstThursday = firstThursdayOfYear(year)

  let weekNumber = 1
  let tmpEpoch = firstThursday.getTime()

  if (tmpEpoch > epoch) {
    // We're still in the week started last year
    return getYearlyWeekNumber(new Date(date.getUTCFullYear() - 1, 11, 31).getTime())
  }

  while (true) {
    tmpEpoch += WEEK_EPOCH
    if (tmpEpoch > epoch) {
      return { weekNumber, year }
    }
    weekNumber++
  }
}

module.exports.waitForSchema = async function waitForSchema (itemSchemaModule) {
  while (!itemSchemaModule.isSchemaReady()) {
    await module.exports.wait(2000)
  }
}

module.exports.wait = function wait (ms) { return new Promise(resolve => setTimeout(resolve, ms)) }

module.exports.timeFunction = function timeFunction (fn) {
  return new Promise((resolve, reject) => {
    const start = Date.now()
    const result = fn()
    if (result instanceof Promise) {
      result.then(result => {
        resolve({ result, ms: Date.now() - start })
      }).catch(e => {
        reject(e)
      })
    } else {
      resolve({ result, ms: Date.now() - start })
    }
  })
}
